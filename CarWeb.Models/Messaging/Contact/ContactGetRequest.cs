﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.Messaging.Contact
{
    public class ContactGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

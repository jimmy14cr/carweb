﻿using CarWeb.Models.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.Messaging.Contact
{
    public class ContactAddResponse
    {
        public ContactViewModel Contact
        {
            get;
            set;
        }
    }
}

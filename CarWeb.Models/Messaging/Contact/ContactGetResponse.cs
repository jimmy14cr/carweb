﻿using CarWeb.Models.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.Messaging.Contact
{
    public class ContactGetResponse
    {
        public ContactViewModel Contact
        {
            get;
            set;
        }
    }
}

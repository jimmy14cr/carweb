﻿using CarWeb.Models.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.Messaging.Contact
{
    public class ContactGetListResponse
    {
        public IEnumerable<ContactViewModel> Contacts { get; set; }
    }
}

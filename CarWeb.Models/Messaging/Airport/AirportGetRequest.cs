﻿using System;


namespace CarWeb.Models.Messaging.Airport
{
    public class AirportGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

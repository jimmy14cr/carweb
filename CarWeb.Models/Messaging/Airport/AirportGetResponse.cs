﻿using CarWeb.Models.ViewModels.Airport;

namespace CarWeb.Models.Messaging.Airport
{
    public class AirportGetResponse
    {
        public AirportViewModel Airport
        {
            get;
            set;
        }
    }
}

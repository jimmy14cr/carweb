﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Airport;

namespace CarWeb.Models.Messaging.Airport
{
    public class AirportAddResponse
    {
        public AirportViewModel Airport
        {
            get;
            set;
        }
    }
}

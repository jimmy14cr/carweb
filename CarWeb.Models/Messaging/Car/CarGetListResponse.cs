using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Car;

namespace CarWeb.Models.Messaging.Car
{
    public class CarGetListResponse
    {
        public IEnumerable<CarViewModel> Cars { get; set; }
    }
}

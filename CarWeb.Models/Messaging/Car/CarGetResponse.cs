﻿using CarWeb.Models.ViewModels.Car;

namespace CarWeb.Models.Messaging.Car
{
    public class CarGetResponse
    {
        public CarViewModel Car
        {
            get;
            set;
        }
    }
}

﻿using System;


namespace CarWeb.Models.Messaging.Car
{
    public class CarGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

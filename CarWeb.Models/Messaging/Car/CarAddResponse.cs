﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Car;

namespace CarWeb.Models.Messaging.Car
{
    public class CarAddResponse
    {
        public CarViewModel Car
        {
            get;
            set;
        }
    }
}

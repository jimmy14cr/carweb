﻿using System;


namespace CarWeb.Models.Messaging.Terminal
{
    public class TerminalGetRequest
    {
        public Guid Id { get; set; }
        public string AirportCode { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Terminal;

namespace CarWeb.Models.Messaging.Terminal
{
    public class TerminalGetListResponse
    {
        public IEnumerable<TerminalViewModel> Terminals { get; set; }
    }
}

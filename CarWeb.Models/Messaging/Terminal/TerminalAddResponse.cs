﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Terminal;

namespace CarWeb.Models.Messaging.Terminal
{
    public class TerminalAddResponse
    {
        public TerminalViewModel Terminal
        {
            get;
            set;
        }
    }
}

﻿using CarWeb.Models.ViewModels.Terminal;

namespace CarWeb.Models.Messaging.Terminal
{
    public class TerminalGetResponse
    {
        public TerminalViewModel Terminal
        {
            get;
            set;
        }
    }
}

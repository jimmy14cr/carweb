
﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Terminal;

namespace CarWeb.Models.Messaging.Terminal
{
    public class TerminalAddRequest
    {
        public TerminalViewModel Terminal
        {
            get;
            set;
        }
    }
}

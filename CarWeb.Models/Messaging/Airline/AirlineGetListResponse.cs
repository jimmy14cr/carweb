using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Airline;

namespace CarWeb.Models.Messaging.Airline
{
    public class AirlineGetListResponse
    {
        public IEnumerable<AirlineViewModel> Airlines { get; set; }
    }
}

﻿using CarWeb.Models.ViewModels.Airline;

namespace CarWeb.Models.Messaging.Airline
{
    public class AirlineGetResponse
    {
        public AirlineViewModel Airline
        {
            get;
            set;
        }
    }
}

﻿using System;


namespace CarWeb.Models.Messaging.Airline
{
    public class AirlineGetRequest
    {
        public Guid Id { get; set; }
        public Guid TerminalId { get; set; }
    }
}

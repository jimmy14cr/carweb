
﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.ServiceType;

namespace CarWeb.Models.Messaging.ServiceType
{
    public class ServiceTypeAddRequest
    {
        public ServiceTypeViewModel ServiceType
        {
            get;
            set;
        }
    }
}

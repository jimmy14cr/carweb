﻿using System;


namespace CarWeb.Models.Messaging.ServiceType
{
    public class ServiceTypeGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

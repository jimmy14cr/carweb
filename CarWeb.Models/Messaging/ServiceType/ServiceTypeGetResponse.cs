﻿using CarWeb.Models.ViewModels.ServiceType;

namespace CarWeb.Models.Messaging.ServiceType
{
    public class ServiceTypeGetResponse
    {
        public ServiceTypeViewModel ServiceType
        {
            get;
            set;
        }
    }
}

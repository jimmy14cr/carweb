﻿using System;


namespace CarWeb.Models.Messaging.CountryCode
{
    public class CountryCodeGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using CarWeb.Models.ViewModels.CountryCode;

namespace CarWeb.Models.Messaging.CountryCode
{
    public class CountryCodeGetResponse
    {
        public CountryCodeViewModel CountryCode
        {
            get;
            set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.CountryCode;

namespace CarWeb.Models.Messaging.CountryCode
{
    public class CountryCodeAddResponse
    {
        public CountryCodeViewModel CountryCode
        {
            get;
            set;
        }
    }
}

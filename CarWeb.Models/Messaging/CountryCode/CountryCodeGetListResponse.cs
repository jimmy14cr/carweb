using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.CountryCode;

namespace CarWeb.Models.Messaging.CountryCode
{
    public class CountryCodeGetListResponse
    {
        public IEnumerable<CountryCodeViewModel> CountryCodes { get; set; }
    }
}

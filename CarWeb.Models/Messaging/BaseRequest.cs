﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models
{
    public class BaseRequest
    {       
        public Guid Id { get; set; }
        public Guid FilterId { get; set; }
        public string IdCustomer { get; set; }
        public string IdSeller { get; set; }
        public Guid IdUser { get; set; }
        public string Name { get; set; }
        public string DepartureDate { get; set; }
        public string ArriveDate { get; set; }
    }
}

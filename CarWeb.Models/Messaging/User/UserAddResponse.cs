﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.User;

namespace CarWeb.Models.Messaging.User
{
    public class UserAddResponse
    {
        public UserViewModel User
        {
            get;
            set;
        }
    }
}

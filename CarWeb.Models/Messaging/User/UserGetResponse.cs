﻿using CarWeb.Models.ViewModels.User;

namespace CarWeb.Models.Messaging.User
{
    public class UserGetResponse
    {
        public UserViewModel User
        {
            get;
            set;
        }
    }
}

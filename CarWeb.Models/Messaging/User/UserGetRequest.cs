﻿using System;


namespace CarWeb.Models.Messaging.User
{
    public class UserGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}

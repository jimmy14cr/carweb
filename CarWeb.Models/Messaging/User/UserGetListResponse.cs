using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.User;

namespace CarWeb.Models.Messaging.User
{
    public class UserGetListResponse
    {
        public IEnumerable<UserViewModel> Users { get; set; }
    }
}

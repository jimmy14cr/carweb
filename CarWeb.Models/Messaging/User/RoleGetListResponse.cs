using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Role;
using CarWeb.Models.ViewModels.User;
using CarWeb.Models.ViewModels.UserRole;

namespace CarWeb.Models.Messaging.User
{
    public class RoleGetListResponse
    {
        public IEnumerable<RoleViewModel> Roles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            IsValid = true;
        }
        public bool IsValid { get; set; }

        public string[] ErrorMessages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using CarWeb.Models.ViewModels.Reservation;

namespace CarWeb.Models.Messaging.Reservation
{
    public class ReservationAddResponse
    {
        public ReservationViewModel Reservation
        {
            get;
            set;
        }
    }
}

﻿using CarWeb.Models.ViewModels.Reservation;

namespace CarWeb.Models.Messaging.Reservation
{
    public class ReservationPreviewGetResponse
    {
        public ReservationPreviewViewModel ReservationPreview
        {
            get;
            set;
        }
    }
}

﻿using System;


namespace CarWeb.Models.Messaging.Reservation
{
    public class ReservationGetRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

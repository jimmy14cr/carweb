﻿using CarWeb.Models.ViewModels.Reservation;

namespace CarWeb.Models.Messaging.Reservation
{
    public class ReservationGetResponse
    {
        public ReservationViewModel Reservation
        {
            get;
            set;
        }
    }
}

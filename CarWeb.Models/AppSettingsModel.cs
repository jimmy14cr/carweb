﻿using System;

namespace CarWeb.Models
{
    public class AppSettingsModel
    {
        public string SendGridKey
        {
            get;
            set;
        }

        public WebServices WebServices
        {
            get;
            set;
        }

    }
    

    public class WebServices
    {

        public EndPoints EndPoints
        {
            get;
            set;
        }
    }

    public class EndPoints
    {
        public string CarWeb
        {
            get;
            set;
        }

    }
}

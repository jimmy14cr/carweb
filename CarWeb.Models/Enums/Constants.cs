﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.Enums
{
    public class Constants
    {
        public enum StatusReservation
        {
            [StringValue("CT")]
            Charter = 1,
            [StringValue("T")]
            Transfer = 2,
            [StringValue("TA")]
            To_Airport = 3,
            [StringValue("FA")]
            From_Airport = 3,
            [StringValue("PTP")]
            Point_to_point = 4,
            [StringValue("HD")]
            Hourly_Directed = 5,
            [StringValue("O")]
            Other = 6,
            [StringValue("RT")]
            Round_Trip = 7
        }
    }
}

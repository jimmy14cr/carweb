﻿namespace CarWeb.Models.ViewModels.DataTable
{
    public class DataTableModelOrder
    {
        public int Column { get; set; }
        public string Dir { get; set; }

    }
}

﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Role
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int? Code { get; set; }
    }
}

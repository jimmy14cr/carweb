﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarWeb.Models.ViewModels.Contact
{
    public class ContactView
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required.")]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone Number is required.")]
        public string PhoneNumber { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message is required.")]
        public string Message { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Contact
{
    public class ContactViewModel
    {
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
    }
}

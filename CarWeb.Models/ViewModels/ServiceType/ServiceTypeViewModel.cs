﻿
using CarWeb.Models.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.ServiceType
{
    public class ServiceTypeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

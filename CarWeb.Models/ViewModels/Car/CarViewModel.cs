﻿
using CarWeb.Models.ViewModels.Image;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Car
{
    public class CarViewModel
    {
        public CarViewModel()
        {
            ImageCars = new HashSet<ImageCarViewModel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public short Calification { get; set; }
        public short Door { get; set; }
        public short Luggage { get; set; }
        public short Seat { get; set; }
        public double Price { get; set; }
        public string Transmission { get; set; }
        public string Brand { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<ImageCarViewModel> ImageCars { get; set; }
    }
}

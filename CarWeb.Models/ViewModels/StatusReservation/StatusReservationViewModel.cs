﻿

using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.StatusReservation
{
    public class StatusReservationViewModel
    {

        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}

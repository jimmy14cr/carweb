﻿using Microsoft.AspNetCore.Mvc.Rendering;
using CarWeb.Models.ViewModels.Airline;
using CarWeb.Models.ViewModels.Airport;
using CarWeb.Models.ViewModels.Car;
using CarWeb.Models.ViewModels.ServiceType;
using CarWeb.Models.ViewModels.StatusReservation;
using CarWeb.Models.ViewModels.Terminal;
using CarWeb.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Reservation
{
    public class ReservationPreviewViewModel
    {
        public Guid Id { get; set; }
        public Guid IdUser { get; set; }
        public Guid IdCar { get; set; }
        public Guid IdStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Description { get; set; }
        public string ServiceTypeCode { get; set; }
        public string serviceTypeCodeName { get; set; }
        public string PickUpLocation { get; set; }
        public string DropOffLocation { get; set; }
        public DateTime? PickUpDate { get; set; }
        public string PickUpTime { get; set; }
        public int? NumberOfPassenger { get; set; }
        public int? LuggageCount { get; set; }
        public decimal? NumberOfHour { get; set; }
        public string AirlineName { get; set; }
        public string AirlineNumber { get; set; }
        public string AirportCode { get; set; }
        public Guid? TerminalId { get; set; }
        public Guid? AirlineId { get; set; }

        public virtual AirlineViewModel Airline { get; set; }
        public virtual AirportViewModel AirportCodeNavigation { get; set; }
        public virtual TerminalViewModel Terminal { get; set; }
        public virtual CarViewModel IdCarNavigation { get; set; }
        public virtual StatusReservationViewModel IdStatusNavigation { get; set; }
        public virtual UserViewModel IdUserNavigation { get; set; }
        public virtual ServiceTypeViewModel ServiceTypeCodeNavigation { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.CountryCode
{
    public class CountryCodeViewModel
    {
        public Guid Id { get; set; }
        public string Country { get; set; }
        public string Code { get; set; }
        public string CodeNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.MailRequest
{
    public class MailRequestView
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
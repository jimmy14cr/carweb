﻿

using CarWeb.Models.ViewModels.Role;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.UserRole
{
    public class UserRoleViewModel
    {
        public Guid Id { get; set; }
        public Guid IdUser { get; set; }
        public Guid IdRole { get; set; }

        public virtual RoleViewModel IdRoleNavigation { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Terminal
{
    public class TerminalViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AirportCode { get; set; }
    }
}

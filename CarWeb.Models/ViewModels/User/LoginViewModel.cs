﻿
using CarWeb.Models.ViewModels.Reservation;
using CarWeb.Models.ViewModels.UserRole;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CarWeb.Models.ViewModels.User
{
    public class LoginViewModel
    {
        //[MaxLength(50, ErrorMessage = "El correo electrónico debe tener un máximo de 50 carácteres")]
        [Required(ErrorMessage = "Email is required")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DisplayName("Password")]
        [MinLength(6, ErrorMessage = "Password must contain more than 6 charaters")]
        //[MaxLength(50, ErrorMessage = "La Contraseña debe contener maximo 20 carácteres")]
        public string Password { get; set; }
    }
}

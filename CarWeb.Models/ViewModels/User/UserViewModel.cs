﻿
using CarWeb.Models.ViewModels.CountryCode;
using CarWeb.Models.ViewModels.Reservation;
using CarWeb.Models.ViewModels.UserRole;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.User
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            Reservations = new HashSet<ReservationViewModel>();
            UserRoles = new HashSet<UserRoleViewModel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsOldUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CountryCode { get; set; }

        public CountryCodeViewModel CountryCodeNavigation { get; set; }
        public virtual ICollection<ReservationViewModel> Reservations { get; set; }
        public virtual ICollection<UserRoleViewModel> UserRoles { get; set; }
    }
}

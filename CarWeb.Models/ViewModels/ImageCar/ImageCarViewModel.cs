﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Image
{
    public class ImageCarViewModel
    {
        public Guid Id { get; set; }
        public Guid IdCar { get; set; }
        public string Path { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Airport
{
    public class AirportViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarWeb.Models.ViewModels.Airline
{
    public class AirlineViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid TerminalId { get; set; }
    }
}

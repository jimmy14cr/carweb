﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using CarWeb.Models;

namespace CarWeb.Services
{
    public class BaseService<TLogger>
    {
        protected readonly RestClient _client;
        //protected readonly IConfiguration _configuration;
        private readonly ILogger<TLogger> _logger;

        public BaseService(IOptions<AppSettingsModel> configuration, ILogger<TLogger> logger)
        {
            _logger = logger;
            //_configuration = configuration;
            var baseUrl = configuration.Value.WebServices.EndPoints.CarWeb;//  _configuration["WebServices:EndPoints:ElectronicBillAPI"];
            _client = new RestClient(baseUrl);
        }


        protected async Task<ContractResponse<R>> CreateRequest<T, R>(ContractRequest<T> request, string url, Method method)
           where T : class
           where R : class
        {

            try
            {
                var requestService = new RestRequest(url, method);
                requestService.AddHeader("Cache-Control", "no-cache");
                requestService.AddHeader("Content-Type", "application/json; charset-utf-8");
                //requestService.AddParameter("Authorization", $"Bearer {request.Token}", ParameterType.HttpHeader);
                //requestService.AddParameter("application/json", request, ParameterType.RequestBody);
                requestService.AddJsonBody(request);
                //var responseService = await _client.ExecuteTaskAsync<ContractResponse<R>>(requestService);
                var responseService = await _client.ExecuteAsync(requestService);

                if (responseService.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //return responseService.Data;
                    var x = Newtonsoft.Json.JsonConvert.DeserializeObject<ContractResponse<R>>(responseService.Content);
                    return x;

                }

                return new ContractResponse<R>
                {
                    IsValid = false,
                    ErrorMessages = new List<string> { "Recurso no encontrado" }
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(20, ex, ex.Message);
                return new ContractResponse<R> { IsValid = false, ErrorMessages = new List<string> { "Error de communicación" } };
            }
        }

    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.Car;

namespace CarWeb.Services.Implementations
{
    public class CarService : BaseService<CarService>, ICarService
    {
        private readonly string module = "car";

        private readonly ILogger<CarService> _logger;

        public CarService(ILogger<CarService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }

        public async Task<ContractResponse<CarGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, CarGetListResponse>(request, module, Method.GET);
        }

        public async Task<ContractResponse<CarGetListResponse>> Get(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, CarGetListResponse>(request, $"{module}/GetById?id={request.Data.Id}", Method.GET);
        }

        public async Task<ContractResponse<CarGetListResponse>> GetByVerify(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, CarGetListResponse>(request, $"{module}/GetByVerify?departureDate={request.Data.DepartureDate}&arriveDate={request.Data.ArriveDate}", Method.GET);
        }

        public async Task<ContractResponse<CarGetResponse>> Add(ContractRequest<CarAddRequest> request)
        {
            return await CreateRequest<CarAddRequest, CarGetResponse>(request, module, Method.POST);

        }
    }
}

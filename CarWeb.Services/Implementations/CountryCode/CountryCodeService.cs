﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.CountryCode;

namespace CarWeb.Services.Implementations
{
    public class CountryCodeService : BaseService<CountryCodeService>, ICountryCodeService
    {
        private readonly string module = "countryCode";

        private readonly ILogger<CountryCodeService> _logger;

        public CountryCodeService(ILogger<CountryCodeService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }

        public async Task<ContractResponse<CountryCodeGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, CountryCodeGetListResponse>(request, module, Method.GET);
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.ServiceType;

namespace CarWeb.Services.Implementations
{
    public class ServiceTypeService : BaseService<ServiceTypeService>, IServiceTypeService
    {
        private readonly string module = "serviceType";

        private readonly ILogger<ServiceTypeService> _logger;

        public ServiceTypeService(ILogger<ServiceTypeService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }

        public async Task<ContractResponse<ServiceTypeGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ServiceTypeGetListResponse>(request, module, Method.GET);
        }
    }
}

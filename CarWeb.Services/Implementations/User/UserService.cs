﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.User;

namespace CarWeb.Services.Implementations
{
    public class UserService : BaseService<UserService>, IUserService
    {
        private readonly string module = "user";

        private readonly ILogger<UserService> _logger;

        public UserService(ILogger<UserService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }
        public async Task<ContractResponse<UserGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, UserGetListResponse>(request, module, Method.GET);
        }

        public async Task<ContractResponse<UserGetListResponse>> Get(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, UserGetListResponse>(request, $"{module}/BySellerAndCustomer?id={request.Data.IdSeller}&CustomerIdentification={request.Data.IdCustomer}", Method.GET);
        }

        public async Task<ContractResponse<UserGetResponse>> GetById(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, UserGetResponse>(request, $"{module}/GetById?id={request.Data.Id}", Method.GET);
        }

        public async Task<ContractResponse<UserGetResponse>> GetByEmail(ContractRequest<UserGetRequest> request)
        {
            return await CreateRequest<UserGetRequest, UserGetResponse>(request, $"{module}/GetByEmail?email={request.Data.Email}", Method.GET);
        }

        public async Task<ContractResponse<UserGetResponse>> UpdataPassword(ContractRequest<UserAddRequest> request)
        {
            return await CreateRequest<UserAddRequest, UserGetResponse>(request, $"{module}/updatePassword", Method.PUT);
        }

        public async Task<ContractResponse<UserGetResponse>> Add(ContractRequest<UserAddRequest> request)
        {
            return await CreateRequest<UserAddRequest, UserGetResponse>(request, module, Method.POST);
        }

        public async Task<ContractResponse<RoleGetListResponse>> GetAllRoles(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, RoleGetListResponse>(request, $"{module}/getAllRoles", Method.GET);
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.Airport;
using CarWeb.Models.Messaging.Terminal;
using CarWeb.Models.Messaging.Airline;

namespace CarWeb.Services.Implementations
{
    public class AirportService : BaseService<AirportService>, IAirportService
    {
        private readonly string module = "airport";

        private readonly ILogger<AirportService> _logger;

        public AirportService(ILogger<AirportService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }

        public async Task<ContractResponse<AirportGetListResponse>> GetAirports(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, AirportGetListResponse>(request, module, Method.GET);
        }

        public async Task<ContractResponse<TerminalGetListResponse>> GetTerminalsByAirport(ContractRequest<TerminalGetRequest> request)
        {
            return await CreateRequest<TerminalGetRequest, TerminalGetListResponse>(request, $"{module}/GetTerminalByAirportCode?airportCode={request.Data.AirportCode}", Method.GET);
        }

        public async Task<ContractResponse<AirlineGetListResponse>> GetAirlinesByTerminal(ContractRequest<AirlineGetRequest> request)
        {
            return await CreateRequest<AirlineGetRequest, AirlineGetListResponse>(request, $"{module}/GetAirlineByTerminal?terminalId={request.Data.TerminalId}", Method.GET);
        }
    }
}

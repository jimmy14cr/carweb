﻿//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using System.Threading.Tasks;
//using RestSharp;
//using System;
//using Microsoft.Extensions.Options;
//using CarWeb.Services.Interfaces;
//using CarWeb.Models;

//namespace CarWeb.Services.Implementations
//{
//    public class SecurityService : BaseService<SecurityService>, ISecurityService
//    {
//        private readonly string module = "securitys";
//        private readonly ILogger<SecurityService> _logger;

//        public SecurityService(ILogger<SecurityService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
//        {
//            _logger = logger;
//        }

//        public async Task<ContractResponse<UserGetResponse>> GetUser(ContractRequest<BaseRequest> request)
//        {
//            return await CreateRequest<BaseRequest, UserGetResponse>(request, $"{module}/{request.Data.Id}", Method.GET);
//        }

//        public async Task<ContractResponse<UserGetResponse>> GetUserByUserName(ContractRequest<UserGetRequest> request)
//        {
//            return await CreateRequest<UserGetRequest, UserGetResponse>(request, $"{module}/GetByUserName/{request.Data.UserName}", Method.GET);
//        }
//    }
//}

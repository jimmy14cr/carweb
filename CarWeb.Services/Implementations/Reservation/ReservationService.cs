﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using RestSharp;
using System;
using Microsoft.Extensions.Options;
using CarWeb.Services.Interfaces;
using CarWeb.Models;
using CarWeb.Models.Messaging.Reservation;

namespace CarWeb.Services.Implementations
{
    public class ReservationService : BaseService<ReservationService>, IReservationService
    {
        private readonly string module = "reservation";

        private readonly ILogger<ReservationService> _logger;

        public ReservationService(ILogger<ReservationService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }
        public async Task<ContractResponse<ReservationGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ReservationGetListResponse>(request, $"{module}/GetAll", Method.POST);
        }

        public async Task<ContractResponse<ReservationGetListResponse>> Get(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ReservationGetListResponse>(request, $"{module}/BySellerAndCustomer?id={request.Data.IdSeller}&CustomerIdentification={request.Data.IdCustomer}", Method.GET);
        }

        public async Task<ContractResponse<ReservationPreviewGetResponse>> Preview(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ReservationPreviewGetResponse>(request, $"{module}/Preview?id={request.Data.Id}", Method.GET);
        }

        public async Task<ContractResponse<ReservationGetResponse>> Add(ContractRequest<ReservationAddRequest> request)
        {
            return await CreateRequest<ReservationAddRequest, ReservationGetResponse>(request, module, Method.POST);
        }

        public async Task<ContractResponse<ReservationGetResponse>> Accept(ContractRequest<ReservationAddRequest> request)
        {
            return await CreateRequest<ReservationAddRequest, ReservationGetResponse>(request, $"{module}/accept", Method.POST);
        }

        public async Task<ContractResponse<ReservationGetResponse>> Reject(ContractRequest<ReservationAddRequest> request)
        {
            return await CreateRequest<ReservationAddRequest, ReservationGetResponse>(request, $"{module}/reject", Method.POST);
        }
    }
}

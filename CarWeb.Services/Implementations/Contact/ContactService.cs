﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using CarWeb.Models;
using CarWeb.Models.Messaging.Contact;
using CarWeb.Services.Interfaces.Contact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarWeb.Services.Implementations.Contact
{
    public class ContactService : BaseService<ContactService>, IContactService
    {
        private readonly string module = "contact";

        private readonly ILogger<ContactService> _logger;

        public ContactService(ILogger<ContactService> logger, IOptions<AppSettingsModel> configuration) : base(configuration, logger)
        {
            _logger = logger;
        }
        public async Task<ContractResponse<ContactGetListResponse>> GetAll(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ContactGetListResponse>(request, $"{module}/GetAll", Method.POST);
        }

        public async Task<ContractResponse<ContactGetListResponse>> Get(ContractRequest<BaseRequest> request)
        {
            return await CreateRequest<BaseRequest, ContactGetListResponse>(request, $"{module}/BySellerAndCustomer?id={request.Data.IdSeller}&CustomerIdentification={request.Data.IdCustomer}", Method.GET);
        }

        public async Task<ContractResponse<ContactGetResponse>> Add(ContractRequest<ContactAddRequest> request)
        {
            return await CreateRequest<ContactAddRequest, ContactGetResponse>(request, module, Method.POST);
        }
    }
}

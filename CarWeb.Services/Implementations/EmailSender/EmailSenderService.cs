﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using CarWeb.Models.ViewModels.Contact;
using CarWeb.Models.ViewModels.SmtpSettings;
using CarWeb.Services.Interfaces.EmailSender;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CarWeb.Services.Implementations.EmailSender
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly SmtpSettingsView _smtpSettings;

        public EmailSenderService(IOptions<SmtpSettingsView> smtpSettings)
        {
            _smtpSettings = smtpSettings.Value;
        }

        public async Task SendEmailAsync(ContactView request)
        {
            try
            {
                string FilePath = Directory.GetCurrentDirectory() + "\\Templates\\WelcomeTemplate.html";
                StreamReader str = new StreamReader(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();
                MailText = MailText.Replace("[username]", request.Name).Replace("[email]", request.Email).Replace("[phoneNumber]", request.PhoneNumber).Replace("[message]", request.Message);
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_smtpSettings.SenderEmail);
                email.To.Add(MailboxAddress.Parse(_smtpSettings.SenderEmail));
                email.Subject = $"You have a request for a quote from {request.Name}";
                var builder = new BodyBuilder();
                builder.HtmlBody = MailText;
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(_smtpSettings.Server, _smtpSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_smtpSettings.SenderEmail, _smtpSettings.Password);
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
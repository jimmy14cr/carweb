﻿using System;
using TimeZoneConverter;
using CarWeb.Services.Interfaces;

namespace CarWeb.Services.Implementations
{
    public class TimeZoneService : ITimeZoneService
    {
        public DateTime Now()
        {
            DateTime utcTime = DateTime.UtcNow;
            TimeZoneInfo BdZone = TZConvert.GetTimeZoneInfo("Central America Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utcTime, BdZone);
        }
    }
}

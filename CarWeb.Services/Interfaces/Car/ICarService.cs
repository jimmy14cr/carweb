﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.Car;

namespace CarWeb.Services.Interfaces
{
    public interface ICarService
    {
        Task<ContractResponse<CarGetListResponse>> Get(ContractRequest<BaseRequest> request);
        Task<ContractResponse<CarGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
        Task<ContractResponse<CarGetResponse>> Add(ContractRequest<CarAddRequest> request);
        Task<ContractResponse<CarGetListResponse>> GetByVerify(ContractRequest<BaseRequest> request);
    }
}

﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.CountryCode;
using CarWeb.Models.Messaging.Airport;
using CarWeb.Models.Messaging.Terminal;
using CarWeb.Models.Messaging.Airline;

namespace CarWeb.Services.Interfaces
{
    public interface IAirportService
    {
        Task<ContractResponse<AirportGetListResponse>> GetAirports(ContractRequest<BaseRequest> request);
        Task<ContractResponse<TerminalGetListResponse>> GetTerminalsByAirport(ContractRequest<TerminalGetRequest> request);
        Task<ContractResponse<AirlineGetListResponse>> GetAirlinesByTerminal(ContractRequest<AirlineGetRequest> request);
    }
}

﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.CountryCode;

namespace CarWeb.Services.Interfaces
{
    public interface ICountryCodeService
    {
        Task<ContractResponse<CountryCodeGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
    }
}

﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.Reservation;

namespace CarWeb.Services.Interfaces
{
    public interface IReservationService
    {
        Task<ContractResponse<ReservationGetListResponse>> Get(ContractRequest<BaseRequest> request);
        //Task<ContractResponse<ReservationGetListResponse>> GetById(ContractRequest<BaseRequest> request);
        Task<ContractResponse<ReservationGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
        Task<ContractResponse<ReservationGetResponse>> Add(ContractRequest<ReservationAddRequest> request);
        Task<ContractResponse<ReservationGetResponse>> Accept(ContractRequest<ReservationAddRequest> request);
        Task<ContractResponse<ReservationGetResponse>> Reject(ContractRequest<ReservationAddRequest> request);
        Task<ContractResponse<ReservationPreviewGetResponse>> Preview(ContractRequest<BaseRequest> request);
    }
}

﻿using CarWeb.Models;
using CarWeb.Models.Messaging.Contact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarWeb.Services.Interfaces.Contact
{
    public interface IContactService
    {
        Task<ContractResponse<ContactGetListResponse>> Get(ContractRequest<BaseRequest> request);
        Task<ContractResponse<ContactGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
        Task<ContractResponse<ContactGetResponse>> Add(ContractRequest<ContactAddRequest> request);
    }
}

﻿using CarWeb.Models.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CarWeb.Services.Interfaces.EmailSender
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(ContactView request);
    }
}

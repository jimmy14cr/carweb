﻿using System;
namespace CarWeb.Services.Interfaces
{
    public interface ITimeZoneService
    {
        DateTime Now();
    }
}

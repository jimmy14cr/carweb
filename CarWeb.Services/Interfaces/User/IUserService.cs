﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.User;

namespace CarWeb.Services.Interfaces
{
    public interface IUserService
    {
        Task<ContractResponse<UserGetListResponse>> Get(ContractRequest<BaseRequest> request);
        Task<ContractResponse<UserGetResponse>> GetById(ContractRequest<BaseRequest> request);
        Task<ContractResponse<UserGetResponse>> GetByEmail(ContractRequest<UserGetRequest> request);
        Task<ContractResponse<UserGetResponse>> UpdataPassword(ContractRequest<UserAddRequest> request);
        Task<ContractResponse<UserGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
        Task<ContractResponse<UserGetResponse>> Add(ContractRequest<UserAddRequest> request);
        Task<ContractResponse<RoleGetListResponse>> GetAllRoles(ContractRequest<BaseRequest> request);
    }
}

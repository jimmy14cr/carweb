﻿using CarWeb.Models;
using System;
using System.Threading.Tasks;
using CarWeb.Models.Messaging.ServiceType;

namespace CarWeb.Services.Interfaces
{
    public interface IServiceTypeService
    {
        Task<ContractResponse<ServiceTypeGetListResponse>> GetAll(ContractRequest<BaseRequest> request);
    }
}

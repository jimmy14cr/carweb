﻿using CarWeb.Models.Car;
using CarWeb.Models.StatusReservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.Reservation
{
    public class ReservationView
    {
        public Guid Id { get; set; }
        public Guid IdUser { get; set; }
        public Guid IdCar { get; set; }
        public Guid IdStatus { get; set; }
        public string DeparturePlace { get; set; }
        public string ArrivePlace { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArriveDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Description { get; set; }
        public Guid TerminalId { get; set; }
        public Guid AirlineId { get; set; }
        public virtual CarView IdCarNavigation { get; set; }
        public virtual StatusReservationView IdStatusNavigation { get; set; }
    }
}

﻿using CarWeb.Models.Reservation;
using CarWeb.Models.UserRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.User
{
    public class UserView
    {
        public UserView()
        {
            Reservation = new HashSet<ReservationView>();
            UserRole = new HashSet<UserRoleView>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Identification { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<ReservationView> Reservation { get; set; }
        public virtual ICollection<UserRoleView> UserRole { get; set; }
    }
}

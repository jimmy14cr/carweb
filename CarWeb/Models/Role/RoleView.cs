﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.Role
{
    public class RoleView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}

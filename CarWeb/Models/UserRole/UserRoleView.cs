﻿using CarWeb.Models.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.UserRole
{
    public class UserRoleView
    {
        public Guid Id { get; set; }
        public Guid IdUser { get; set; }
        public Guid IdRole { get; set; }

        public virtual RoleView IdRoleNavigation { get; set; }
    }
}

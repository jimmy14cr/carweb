﻿namespace CarWeb.Models.DataTable
{
    public class DataTableModelSearch
    {
        public bool Regex { get; set; }
        public string Value { get; set; }
        
    }
}
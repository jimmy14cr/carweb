﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.ImageCar
{
    public class ImageCarView
    {
        public Guid Id { get; set; }
        public Guid IdCar { get; set; }
        public string Path { get; set; }
    }
}

﻿
using AutoMapper;
using CarWeb.Models.Car;
using CarWeb.Models.Reservation;
using CarWeb.Models.User;
using CarWeb.Models.ViewModels.Car;
using CarWeb.Models.ViewModels.Contact;
using CarWeb.Models.ViewModels.Reservation;
using CarWeb.Models.ViewModels.User;

namespace CarWeb.Models
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<UserView, UserViewModel>().ReverseMap();
            CreateMap<ReservationView, ReservationViewModel>().ReverseMap();
            CreateMap<CarView, CarViewModel>().ReverseMap();
            CreateMap<ContactView, ContactViewModel>().ReverseMap();
        }
    }
}

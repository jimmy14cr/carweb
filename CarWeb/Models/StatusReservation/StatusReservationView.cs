﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.StatusReservation
{
    public class StatusReservationView
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}

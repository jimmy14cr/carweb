﻿using CarWeb.Models.ImageCar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Models.Car
{
    public class CarView
    {
        public CarView()
        {
            ImageCar = new HashSet<ImageCarView>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public short Calification { get; set; }
        public short Door { get; set; }
        public short Luggage { get; set; }
        public short Seat { get; set; }
        public double Price { get; set; }
        public string Transmission { get; set; }
        public string Brand { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<ImageCarView> ImageCar { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace CarWeb.Security
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        // public Guid Id { get; set; }
        public string EncriptedPassword { get; set; }

        public IList<string> Roles { get; set; }
    }
}

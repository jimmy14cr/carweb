﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Linq;
using AutoMapper;
using CarWeb.Services.Interfaces;
using CarWeb.Models.ViewModels.User;
using CarWeb.Encryption2;
using CarWeb.Models;
using CarWeb.Models.Messaging.User;

namespace CarWeb.Security
{
    internal class ApplicationUserStore : 
        IUserStore<ApplicationUser>,
        IUserRoleStore<ApplicationUser>
        ,IUserPasswordStore<ApplicationUser>
        ,IUserSecurityStampStore<ApplicationUser>
    {
        private readonly IUserService  _userService;
        private readonly ILogger<ApplicationUserStore> _logger; 
        private readonly IMapper _mapper;

        public ApplicationUserStore(ILogger<ApplicationUserStore> logger, IUserService userService, IMapper mapper) 
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
        }

        public void Dispose()
        {

        }

        public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult<string>(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.Email);

        }

        public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
        
        public Task<ApplicationUser> FindByIdAsync(string Id, CancellationToken cancellationToken)
        {
            var userResponse = _userService.GetById(new ContractRequest<BaseRequest> { Data = new BaseRequest { Id = Guid.Parse(Id) } }).GetAwaiter().GetResult();
                //.GetUser(new ContractRequest<UserGetRequest> { Data = new UserGetRequest {Id = Guid.Parse(Id) } });

            if (userResponse == null)
                return Task.FromResult<ApplicationUser>(null);

            var model = _mapper.Map<UserViewModel>(userResponse.Data.User);

            return Task.FromResult(UserToApplicationUser(model));
        }

        public Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var userResponse = _userService.GetByEmail(new ContractRequest<UserGetRequest> { Data = new UserGetRequest { Email = normalizedUserName } }).GetAwaiter().GetResult();

            if (userResponse.Data.User == null)
                return Task.FromResult<ApplicationUser>(null);

            var model = _mapper.Map<UserViewModel>(userResponse.Data.User);

            return Task.FromResult(UserToApplicationUser(model));
        }

       

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.PasswordHash = user.EncriptedPassword;
            return Task.FromResult(user.PasswordHash);
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task AddToRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Roles);

        }


        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            var result = user.Roles != null && user.Roles.Any(x => x == roleName);
            return Task.FromResult(result);
        }

        public Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }


        private ApplicationUser UserToApplicationUser(UserViewModel user)
        {

            var roles = GetRoles(user).Result;

            var applicationUser = new ApplicationUser
            {
                Id = user.Id,
                Email = user.Email,
                SecurityStamp = user.Password,
                Roles = roles,
                EmailConfirmed = true,
            };

            var passHasher = new PasswordHasher<ApplicationUser>();
            var password = Encryption.DecryptTripleDes(user.Password);

            applicationUser.PasswordHash = passHasher.HashPassword(applicationUser, password);

            return applicationUser;
        }

        private Task<IList<string>> GetRoles(UserViewModel user)
        {

            var allRoles = _userService.GetAllRoles(new ContractRequest<BaseRequest>()).GetAwaiter().GetResult();
            var userl = _userService.GetById(new ContractRequest<BaseRequest> { Data = new BaseRequest { Id = user.Id } }).GetAwaiter().GetResult();
            IList<string> roles = new List<string>();
            
            foreach (var role in allRoles.Data.Roles)
            {
                foreach (var roleUser in userl.Data.User.UserRoles)
                {
                    if (roleUser.IdRole == role.Id)
                    {
                        roles.Add(role.Name);
                    }
                }
            }

            return Task.FromResult(roles);

        }

        public Task SetSecurityStampAsync(ApplicationUser user, string stamp, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.SecurityStamp = stamp;
            return Task.FromResult(user.SecurityStamp);
        }

        public Task<string> GetSecurityStampAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            //ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult(user.SecurityStamp);
        }
    }
}

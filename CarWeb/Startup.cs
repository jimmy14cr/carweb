using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CarWeb.Models;
using CarWeb.Models.ViewModels.SmtpSettings;
using CarWeb.Security;
using CarWeb.Services.Implementations;
using CarWeb.Services.Implementations.EmailSender;
using CarWeb.Services.Interfaces;
using CarWeb.Services.Interfaces.EmailSender;
using CarWeb.Services.Implementations.Contact;
using CarWeb.Services.Interfaces.Contact;
using System;
//initial commit
namespace CarWeb
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettingsModel>(Configuration);
            services.AddAutoMapper(c => c.AddProfile<AutoMapping>(), typeof(Startup));
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddScoped(typeof(ICarService), typeof(CarService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IReservationService), typeof(ReservationService));
            services.AddScoped(typeof(IServiceTypeService), typeof(ServiceTypeService));
            services.AddScoped(typeof(ICountryCodeService), typeof(CountryCodeService));
            services.AddScoped(typeof(IAirportService), typeof(AirportService));
            services.AddScoped(typeof(IContactService), typeof(ContactService));

            services.AddIdentity<ApplicationUser, ApplicationRole>().AddDefaultTokenProviders();
            services.AddTransient<IUserStore<ApplicationUser>, ApplicationUserStore>();
            services.AddTransient<IUserRoleStore<ApplicationUser>, ApplicationUserStore>();
            services.AddTransient<IUserPasswordStore<ApplicationUser>, ApplicationUserStore>();
            services.AddTransient<IUserSecurityStampStore<ApplicationUser>, ApplicationUserStore>();
            services.AddTransient<IRoleStore<ApplicationRole>, ApplicationRoleStore>();
            services.Configure<SmtpSettingsView>(Configuration.GetSection("SmtpSettings"));
            services.AddSingleton<IEmailSenderService, EmailSenderService>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                //options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                //options.Cookie.HttpOnly = true;
                options.Cookie = new CookieBuilder
                {
                    HttpOnly = true,
                    Path = "/",
                    SameSite = SameSiteMode.Lax,
                    SecurePolicy = CookieSecurePolicy.SameAsRequest,

                };
                options.LoginPath = "/Security/Login"; // If the LoginPath is not set here, ASP.NET Core will default to /Account/Login
                options.LogoutPath = "/Security/Logout"; // If the LogoutPath is not set here, ASP.NET Core will default to /Account/Logout
                options.AccessDeniedPath = "/Security/AccessDenied"; // If the AccessDeniedPath is not set here, ASP.NET Core will default to /Account/AccessDenied                
                options.ExpireTimeSpan = TimeSpan.FromMinutes(20);

            });

            services.AddSession();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

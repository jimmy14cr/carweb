var phonenumberFormat = (function () {

    $(document).ready(function () {
        var cleave = new Cleave('#clientPhoneNumber', {
            phone: true,
            phoneRegionCode: 'US'
        });

        $("#selectCountryCode").change(function (e) {
            cleave.setPhoneRegionCode(this.value);
            cleave.setRawValue('');
        });
    });


    async function getLocalStorageData(item) {
        return await localStorage.getItem(item);
    }

    return {
    };

}());
﻿var serviceTypeEnum = (function () {

    $(document).ready(function () {

    });

    const serviceType = {
        CHARTER: 'CT',
        TRANSFER: 'T',
        FROM_AIRPORT: 'FA',
        TO_AIRPORT: 'TA',
        HOURLY_DIRECTED: 'HD',
        OTHER: 'O'
    };

    return {
        serviceType: serviceType
    };
}());
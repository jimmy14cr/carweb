var helper = (function () {

    $(document).ready(function () {

    });


    async function getLocalStorageData(item) {
        return await localStorage.getItem(item);
    }

    async function setLocalStorageData(item, data) {
        await localStorage.setItem(item, data);
    }

    async function destroyLocalStorageData(item) {
        await localStorage.removeItem(item);
    }

    async function showInfo(title, message) {
        $(".infoMessage").removeAttr('hidden');
        $(".infoMessage").show();
        $(".infoTitle").text(title);
        $(".infoDetail").text(message);
    }

    async function showInfo2(title, message) {
        $(".infoMessage").removeAttr('hidden');
        $(".infoMessage").show();
        $(".infoTitle").text(title);
        $(".infoDetail").text(message);
    }

    async function showInfoReservations(title, message, id) {
        $(".infoMessage2").removeAttr('hidden');
        $(".infoMessage2").show();
        $(".infoTitle2").text(title);
        $(".infoDetail2").text(message);
        $(".infoBtnReservations").attr("href", `https://localhost:44385/Notification`);
    }

    async function showInfoContacts(title, message, id) {
        $(".infoMessage2").removeAttr('hidden');
        $(".infoMessage2").show();
        $(".infoTitle2").text(title);
        $(".infoDetail2").text(message);
    }

    return {
        getLocalStorageData: getLocalStorageData,
        setLocalStorageData: setLocalStorageData,
        destroyLocalStorageData: destroyLocalStorageData,
        showInfo: showInfo,
        showInfo2: showInfo2,
        showInfoReservations: showInfoReservations,
        showInfoContacts: showInfoContacts
    };

}());
﻿var contact = (function () {

    var objValitation = {
        FirstName: false,
        PhoneNumber: false,
        Email: false,
        Message: false
    };

    $(document).ready(async function () {
        main();
    });

    async function validateInputs() {
        if ($("#firstName").val().length > 0) {
            objValitation.FirstName = true;
            $('#msgFirstName').text("");
        } else {
            objValitation.FirstName = false;
            $('#msgFirstName').text("First name is required");
        }

        if ($("#phoneNumber").val().length > 0) {
            objValitation.PhoneNumber = true;
            $('#msgPhoneNumber').text("");
        } else {
            objValitation.PhoneNumber = false;
            $('#msgPhoneNumber').text("Phone number is required");
        }

        var re = /\S+@\S+\.\S+/;
        if (re.test($("#email").val())) {
            if ($("#email").val().length > 0) {
                objValitation.Email = true;
                $('#msgEmail').text("");
            } else {
                objValitation.Email = false;
                $('#msgEmail').text("Email is required");
            }
        } else {
            objValitation.EmailValidation = false;
            $('#msgEmail').text("Email format example@mail.com");
        }

        if ($("#message").val().length > 0) {
            objValitation.Message = true;
            $('#msgMessage').text("");
        } else {
            objValitation.Message = false;
            $('#msgMessage').text("Message is required");
        }
        if (objValitation.FirstName && objValitation.PhoneNumber && objValitation.Email && objValitation.Message) {
            return true;
        } else {
            return false;
        }
    }


    async function main() {

        $("#saveContactInformation").click(async function () {
            var firstName = $("#firstName").val();
            var phoneNumber = $("#phoneNumber").val();
            var email = $("#email").val();
            var message = $("#message").val();

            if (await validateInputs()) {
                var contactInformation =
                {
                    FirstName: firstName,
                    PhoneNumber: phoneNumber,
                    Email: email,
                    Message: message
                };
                await helper.setLocalStorageData("contactInformation", JSON.stringify(contactInformation));
                verifyContact(contactInformation);
            }
        });
    }

    $(".infoBtn").click(function () {
        $(".infoMessage").hide();
    });
    $(".infoBtn2").click(function () {
        $(".infoMessage2").hide();
    });

    async function verifyContact(contactInformation) {
        console.log(contactInformation)

        console.log(JSON.stringify(contactInformation));
        await $.ajax({

            url: resolveUrl("~/Contact/AddContact"),
            data: JSON.stringify(contactInformation),
            contentType: 'application/json',
            type: "POST",
            dataType: "html",
            success: function (data) {
                JSON.parse(data).id == '00000000-0000-0000-0000-000000000000' ?
                    helper.showInfo2("Information.", "Your contact information was not processed. Please send us an email!") :
                    helper.showInfoContacts("Information.", "Your contact information has been sent!", JSON.parse(data).id);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
}());
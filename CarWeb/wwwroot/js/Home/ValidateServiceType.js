﻿var validateServiceType = (function () {

    $(document).ready(function () {
        initialize();
    });

    async function initialize() {

        $("#selectServiceTypeCode").change(function () {
            var serviceTypeCode = $(this).children("option:selected").val();

            $("#dropOffTitle").text("Location");
            //$("#numberOfHours").hide();
            $(".AirlineNameAndNumber").hide();
            $(".isToAirport").hide();
            $(".isFromAirport").hide();
            $(".isDropOffLocation").show();
            $(".isPickUpLocation").show();
            CleanInputWhenChangingService();

            switch (serviceTypeCode) {
                case serviceTypeEnum.serviceType.CHARTER:
                    //$("#numberOfHours").removeAttr("hidden");
                    //$("#numberOfHours").show();
                    break;
                case serviceTypeEnum.serviceType.TRANSFER:
                    // code block
                    break;
                case serviceTypeEnum.serviceType.FROM_AIRPORT:
                    $(".AirlineNameAndNumber").removeAttr("hidden");
                    $(".AirlineNameAndNumber").show();
                    $(".isFromAirport").removeAttr("hidden");
                    $(".isFromAirport").show();
                    $(".isPickUpLocation").hide();
                    break;
                case serviceTypeEnum.serviceType.TO_AIRPORT:
                    $(".AirlineNameAndNumber").removeAttr("hidden");
                    $(".AirlineNameAndNumber").show();
                    $(".isToAirport").removeAttr("hidden");
                    $(".isToAirport").show();
                    $(".isDropOffLocation").hide();
                    break;
                case serviceTypeEnum.serviceType.HOURLY_DIRECTED:
                    // code block
                    break;
                case serviceTypeEnum.serviceType.OTHER:
                    // code block
                    break;
                default:
            }
        });
    }

    function CleanInputWhenChangingService() {
        $("#reservationAerolineName").val('');
        $("#reservationAerolineNameId").val(null);

        $("#reservationTerminalName").val('');
        $("#reservationTerminalNameId").val(null);
        $("#fromAirportInformation").val('');
        $("#fromAirportId").val(null);
        $("#toAirportInformation").val('');
        $("#toAirportId").val(null);
    }
}());
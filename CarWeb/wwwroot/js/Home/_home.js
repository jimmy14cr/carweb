﻿var home = (function () {
    var from = 'from';
    var go = 'go';
    var stepper = new Stepper($('.bs-stepper')[0]);
    var dataUser = '';

    var objValitation = {
        NameValidation: false,
        NumberValidation: false,
        EmailValidation: false,
        IdentificationValidation: false
    };
    var objValitationTrip = {
        FromValidation: false,
        GoValidation: false,
        DateValidation: false,
        TimeValidation: false,
        PassengerValidation: false,
        PassengerValidation: false,
        LuggageValidation: false,
        NotesValidation: false
    };

    $(document).ready(async function () {
        googleMaps(from);
        googleMaps(go);
        main();
        fill();
        loadCountryCodes();
        siteCarousel();

        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; 
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        document.getElementById("pickUpDate").setAttribute("min", today);
        $("#selectServiceTypeCode").val("T");
    });


    var siteCarousel = function () {
        if ($('.nonloop-block-13').length > 0) {
            $('.nonloop-block-13').owlCarousel({
                center: false,
                items: 1,
                loop: true,
                stagePadding: 0,
                margin: 20,
                smartSpeed: 2500,
                autoplay: false,
                nav: true,
                responsive: {
                    600: {
                        margin: 20,
                        nav: true,
                        items: 2
                    },
                    1000: {
                        margin: 20,
                        stagePadding: 0,
                        nav: true,
                        items: 3
                    }
                }
            });
            $('.custom-next').click(function (e) {
                e.preventDefault();
                $('.nonloop-block-13').trigger('next.owl.carousel');
            })
            $('.custom-prev').click(function (e) {
                e.preventDefault();
                $('.nonloop-block-13').trigger('prev.owl.carousel');
            })
        }

    };

    async function loadCountryCodes() {
        var personalInformation = JSON.parse(await helper.getLocalStorageData("personalInformation"));
        $.getJSON(resolveUrl("~/Home/CountryCodeList"), function (data) {
            var $dropdown = $("#selectCountryCode");
            var $number = $("#clientPhoneNumber");
            $.each(data.data, function (key, val) {
                var isUSA = val.code === 'US' ? 'selected' : '';
                $dropdown.append($(`<option ${isUSA} />`).val(val.code).text(`${val.country} ${val.codeNumber}`));

            });
            $dropdown.val((personalInformation.CountryCode == null ? 'US' : personalInformation.CountryCode));

            $number.val(personalInformation.Number);
        });
    }

    async function validateInputs() {
        if ($("#clientFullname").val().length > 0) {
            objValitation.NameValidation = true;
            $('#msgName').text("");
        } else {
            objValitation.NameValidation = false;
            $('#msgName').text("Name is required");
        }

        if ($("#clientPhoneNumber").val().length > 0) {
            objValitation.NumberValidation = true;
            $('#msgNumber').text("");
        } else {
            objValitation.NumberValidation = false;
            $('#msgNumber').text("Phonenumber is required");
        }

        var re = /\S+@\S+\.\S+/;
        if (re.test($("#clientEmail").val())) {
            if ($("#clientEmail").val().length > 0) {
                objValitation.EmailValidation = true;
                $('#msgEmail').text("");
            } else {
                objValitation.EmailValidation = false;
                $('#msgEmail').text("Email is required");
            }
        } else {
            objValitation.EmailValidation = false;
            $('#msgEmail').text("Email format example@mail.com");
        }

        if (objValitation.NameValidation && objValitation.NumberValidation && objValitation.EmailValidation) {
            return true;
        } else {
            return false;
        }
    }

    async function validateInputsTrip() {
        if ($("#from").val().length > 0) {
            objValitationTrip.FromValidation = true;
            $('#msgFrom').text("");
        } else {
            objValitationTrip.FromValidation = false;
            $('#msgFrom').text("Pickup Address is required");
        }

        if ($("#go").val().length > 0) {
            objValitationTrip.GoValidation = true;
            $('#msgGo').text("");
        } else {
            objValitationTrip.GoValidation = false;
            $('#msgGo').text("Drop-off Address is required");
        }


        var pickUpDate = $("#pickUpDate").val();
        if (pickUpDate.length > 0) {
            objValitationTrip.DateValidation = true;
            $('#msgPickUpDate').text("");
        } else {
            objValitationTrip.DateValidation = false;
            $('#msgPickUpDate').text("Date is required");
        }

        var datepickerTime = $("#pickUpTime").val();
        if (datepickerTime.length > 0) {
            objValitationTrip.TimeValidation = true;
            $('#msgPickUpTime').text("");
        } else {
            objValitationTrip.TimeValidation = false;
            $('#msgPickUpTime').text("Time is required");
        }

        var numberOfPassenger = $("#reservationNumberOfPassenger").val();
        if (numberOfPassenger.length > 0) {
            objValitationTrip.PassengerValidation = true;
            $('#msgNumberOfPassenger').text("");
        } else {
            objValitationTrip.PassengerValidation = false;
            $('#msgNumberOfPassenger').text("Number of passengers is required");
        }

        var reservationLuggageCount = $("#reservationLuggageCount").val();
        if (reservationLuggageCount.length > 0) {
            objValitationTrip.LuggageValidation = true;
            $('#msgLuggageCount').text("");
        } else {
            objValitationTrip.LuggageValidation = false;
            $('#msgLuggageCount').text("Luggage is required");
        }

        var notes = $("#notes").val();
        if (notes.length > 0) {
            objValitationTrip.NotesValidation = true;
            $('#msgNotes').text("");
        } else {
            objValitationTrip.NotesValidation = false;
            $('#msgNotes').text("Notes is required");
        }

        if (objValitationTrip.FromValidation && objValitationTrip.GoValidation && objValitationTrip.DateValidation && objValitationTrip.TimeValidation && objValitationTrip.PassengerValidation && objValitationTrip.LuggageValidation && objValitationTrip.NotesValidation) {
            return true;
        } else {
            return false;
        }
    }
    async function fill() {
        var tripInformation = JSON.parse(await helper.getLocalStorageData("tripInformation"));
        var rentCar = JSON.parse(await helper.getLocalStorageData("rentCar"));
        var personalInformation = JSON.parse(await helper.getLocalStorageData("personalInformation"));

        if (tripInformation != null) {
            $("#from").val(tripInformation.from);
            $("#go").val(tripInformation.go);
            $("#pickUpDate").val(tripInformation.pickUpDate);
            $("#pickUpTime").val(tripInformation.pickUpTime);
            $("#pickUpDate").val(tripInformation.pickUpDate);
            $("#reservationNumberOfPassenger").val(tripInformation.numberOfPassenger);
            $("#reservationLuggageCount").val(tripInformation.luggageCount);
            $("#notes").val(tripInformation.notes);
        }

        if (personalInformation != null) {
            $("#clientFullname").val(personalInformation.Name);
            $("#selectCountryCode").val(personalInformation.CountryCode);
            //$("#identification").val(personalInformation.identification);
            $("#clientPhoneNumber").val(personalInformation.Number);
            $("#clientEmail").val(personalInformation.Email);
            $("#description").val(personalInformation.description);
        }

        if (rentCar != null) {

            $(".rentCar[id=" + JSON.stringify(rentCar.id) + "]").removeClass('btn-primary');
            $(".rentCar[id=" + JSON.stringify(rentCar.id) + "]").addClass('btn-info');
        }
    }


    async function main() {

        $("#saveInformationTrip").click(async function () {
            var from = $("#from").val();
            var go = $("#go").val();
            var serviceTypeCode = $("#selectServiceTypeCode").children("option:selected").val();
            if (serviceTypeCode === serviceTypeEnum.serviceType.FROM_AIRPORT) {
                from = $("#fromAirportInformation").val();
            } else if (serviceTypeCode === serviceTypeEnum.serviceType.TO_AIRPORT) {
                go = $("#toAirportInformation").val();
            }

            var pickUpDate = $("#pickUpDate").val();
            var pickUpTime = $("#pickUpTime").val();
            var reservationTerminalName = $("#reservationTerminalName").val();
            var reservationTerminalNameId = $("#reservationTerminalNameId").val();
            var airlineName = $("#reservationAerolineName").val();
            var airlineNameId = $("#reservationAerolineNameId").val();
            var airlineNumber = $("#reservationAerolineNumber").val();
            var numberOfPassenger = $("#reservationNumberOfPassenger").val();
            var luggageCount = $("#reservationLuggageCount").val();
            var numberOfHour = $("#reservationNumberOfHours").val();
            var airportCode = $("#toAirportId").val();
            var notes = $("#notes").val();

            if (await validateInputsTrip()) {
                var tripInformation =
                {
                    from: from,
                    go: go,
                    pickUpDate: pickUpDate,
                    pickUpTime: pickUpTime,
                    numberOfPassenger: numberOfPassenger,
                    luggageCount: luggageCount,
                    numberOfHour: numberOfHour,
                    airlineName: airlineName,
                    airlineNumber: airlineNumber,
                    airlineNameId: airlineNameId,
                    reservationTerminalName: reservationTerminalName,
                    reservationTerminalNameId: reservationTerminalNameId,
                    airportCode: airportCode,
                    notes : notes
                };
                await helper.setLocalStorageData("tripInformation", JSON.stringify(tripInformation));
                stepper.next();
            }
        });


        $(".item-2").click(function () {
            $("#rating").empty();
            var rentCar = carList.find(c => c.id == $(this).attr("id"));
            console.log(rentCar);
            $("#modalNameCar").text(rentCar.name);
            $("#modalIdCar").val(rentCar.id);

            console.log(rentCar);
            for (var i = rentCar.calification; i > 0; i--) {
                $("#rating").append('<span class="icon-star text-warning"></span>');
            }
            $("#modalPriceCar").text('$' + rentCar.price + '/');
            $("#modalBrandCar").text(rentCar.brand);
            $("#modalDoorCar").text(rentCar.door);
            $("#modalSeatCar").text(rentCar.seat);
            $("#modalTransmissionCar").text(rentCar.transmission);
        });



        $(".savePersonalInformation").click(async function () {
            var clientFullname = $("#clientFullname").val();
            var clientPhoneNumber = $("#clientPhoneNumber").val();
            var clientEmail = $("#clientEmail").val();
            var countryCode = $("#selectCountryCode").val();

            if (await validateInputs()) {
                var personalInformation =
                {
                    Name: clientFullname,
                    Number: clientPhoneNumber,
                    Email: clientEmail,
                    CountryCode: countryCode
                };
                await helper.setLocalStorageData("personalInformation", JSON.stringify(personalInformation));
                var tripInformation = JSON.parse(await helper.getLocalStorageData("tripInformation"));
                var rentCar = JSON.parse(await helper.getLocalStorageData("rentCar"));

                if (tripInformation == null) {
                    await helper.showInfo("Information.", "Please, Complete this information!");
                    stepper.to(1)
                }
                else if (rentCar == null) {
                    await helper.showInfo("Information.", "Please, Choose vehicle!");
                    stepper.to(2)
                }
                else {
                    console.log('Verify Person');
                    verifyPerson(personalInformation, rentCar, tripInformation);
                }
            }
        });

        $(".infoBtn").click(function () {
            $(".infoMessage").hide();
        });
        $(".infoBtn2").click(function () {
            $(".infoMessage2").hide();
        });

        $("#aboutTripId").click(async function () {
            var data = carList;
            if (data != null) {
                stepper.to(1);
            }
        });


        $(".rentCar").click(async function () {
            var rentCarLast = JSON.parse(localStorage.getItem("rentCar"));
            if (rentCarLast != null) {

                $(".rentCar[id=" + JSON.stringify(rentCarLast.id) + "]").removeClass('btn-info');
                $(".rentCar[id=" + JSON.stringify(rentCarLast.id) + "]").addClass('btn-primary');
            }
            var rentCar = carList.find(c => c.id == $(this).attr("id"));
            if (rentCar != null) {

                $(".rentCar[id=" + JSON.stringify(rentCar.id) + "]").removeClass('btn-primary');
                $(".rentCar[id=" + JSON.stringify(rentCar.id) + "]").addClass('btn-info');
            }
            await helper.setLocalStorageData("rentCar", JSON.stringify(rentCar));
            stepper.next();
        });
    }

    async function generatePassword(personalInformation, rentCar, tripInformation) {
        $("#saveUserPassword").click(async function () {
            var password = $("#userPassword").val();
            var id = $("#userId").val();

            var resultPasswordValidated = ValidatePasswordRequirements(password);

            if (!resultPasswordValidated) {
                return;
            }

            var user = {
                Id: id,
                Password: password
            };
            $.ajax({
                url: resolveUrl("~/Home/SaveNewPassword"),
                data: JSON.stringify(user),
                contentType: 'application/json',
                type: "POST",
                dataType: "html",
                success: function (data) {
                    $('#userPasswordModal').modal('hide');
                    $("#userPassword").val("");
                    verifyReservation(dataUser, rentCar, tripInformation);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    }

    function ValidatePasswordRequirements(password) {
        var confirmPassword = $("#confirmUserPassword").val();
        if (password.length < 6) {
            $("#passwordValidationMsg").text("Password must be bigger than 6 digits");
            return false;
        }
        if (confirmPassword != password) {
            $("#confirmPasswordValidationMsg").text("Passwords must be the same");
            return false;
        }
        return true;
    }

    async function verifyPerson(personalInformation, rentCar, tripInformation) {
        $.ajax({
            url: resolveUrl("~/Home/AddPerson"),
            data: JSON.stringify(personalInformation),
            contentType: 'application/json',
            type: "POST",
            dataType: "html",
            success: function (data) {
                var model = JSON.parse(data);
                $("#userEmail").val(model.email);
                if (!model.isOldUser) {
                    $("#userEmail").prop("readonly", true);
                    $("#logUser").hide();
                    $("#saveUserPassword").show();
                    $("#userId").val(model.id);
                    $('#userPasswordModal').modal({ backdrop: 'static', keyboard: false });
                    $('#userPasswordModal').modal({
                        show: true
                    });
                    dataUser = data;
                    generatePassword(dataUser, rentCar, tripInformation);
                } else {
                    dataUser = data;
                    isUserLogged(dataUser, rentCar, tripInformation);
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    async function isUserLogged(dataUser, rentCar, tripInformation) {
        await $.ajax({
            url: resolveUrl("~/Home/IsUserLogged"),
            contentType: 'application/json',
            type: "Get",
            success: function (data) {
                var response = JSON.parse(data);
                if (response) {
                    $("#userPassword").val("");
                    verifyReservation(dataUser, rentCar, tripInformation);
                } else {
                    $('#userPasswordModal').modal({ backdrop: 'static', keyboard: false });
                    $('#userPasswordModal').modal({
                        show: true
                    });
                    $("#userEmail").prop("readonly", false);
                    $("#saveUserPassword").hide();
                    $('#logUser').removeAttr('hidden');
                    $("#logUser").show();
                    login(dataUser, rentCar, tripInformation);
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    async function login(personalInformation, rentCar, tripInformation) {
        $("#logUser").click(async function () {
            var password = $("#userPassword").val();
            var email = $("#userEmail").val();

            var user = {
                Email: email,
                Password: password
            };

            $.ajax({
                url: resolveUrl("~/Home/Login"),
                data: JSON.stringify(user),
                contentType: 'application/json',
                type: "POST",
                dataType: "html",
                success: function (data) {
                    var response = JSON.parse(data);
                    if (response) {
                        $('#userPasswordModal').modal('hide');
                        $("#loginErrorMsg").text("");
                        $("#userPassword").val("");
                        verifyReservation(dataUser, rentCar, tripInformation);
                    } else {
                        $("#loginErrorMsg").text("Email or password incorrect");
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    }

    async function verifyReservation(personalInformation, rentCar, tripInformation) {
        var serviceTypeCode = $("#selectServiceTypeCode").children("option:selected").val();
        var serviceTypeCodeName = $("#selectServiceTypeCode").children("option:selected").text();

        var reservation =
        {
            idUser: JSON.parse(personalInformation).id,
            idCar: rentCar.id,
            idStatus: '09909af9-3d97-407d-9cbb-df4065ac4483',
            pickUpLocation: tripInformation.from,
            dropOffLocation: tripInformation.go,
            pickUpDate: new Date(tripInformation.pickUpDate),
            pickUpTime: tripInformation.pickUpTime,
            numberOfPassenger: parseInt(tripInformation.numberOfPassenger),
            luggageCount: parseInt(tripInformation.luggageCount),
            numberOfHour: parseFloat(0),
            airlineName: tripInformation.airlineName,
            airlineNumber: tripInformation.airlineNumber,
            airlineId: tripInformation.airlineNameId === '' ? null : tripInformation.airlineNameId,
            terminalId: tripInformation.reservationTerminalNameId === '' ? null : tripInformation.reservationTerminalNameId,
            description: $("#description").val(),
            ServiceTypeCode: serviceTypeCode,
            ServiceTypeCodeName: serviceTypeCodeName,
            airportCode: tripInformation.airportCode === '' ? null : tripInformation.airportCode,
            note: tripInformation.notes
        };

        console.log(JSON.stringify(reservation));
        await $.ajax({
            
            url: resolveUrl("~/Home/AddReservation"),
            data: JSON.stringify(reservation),
            contentType: 'application/json',
            type: "POST",
            dataType: "html",
            success: function (data) {
                JSON.parse(data).id == '00000000-0000-0000-0000-000000000000' ?
                    helper.showInfo("Information.", "Your reservation was not processed. Please send us an email!") :
                    helper.showInfoReservations("Information.", "Your reservation has been sent!", JSON.parse(data).id);
            },
            error: function (e) {
                console.log(e)
            }
        });
    }

    async function googleMaps(searchInput) {
        var autocomplete;
        autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
            types: ['geocode'],
        });
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            autocomplete.getPlace();
        });

    }
}());
var airport = (function () {

    $(document).ready(function () {
        getAirport();

        $("#toAirportInformation").click(function () {
            $("#toAirportInformation").autocomplete("search", "");
        });

        $("#toAirportInformation").keyup(function () {
            $("#toAirportId").val(null);
        });

        $("#fromAirportInformation").click(function () {
            $("#fromAirportInformation").autocomplete("search", "");
        });

        $("#fromAirportInformation").keyup(function () {
            $("#fromAirportId").val(null);
        });

        $("#reservationTerminalName").click(function () {
            $("#reservationTerminalName").autocomplete("search", "");
        });

        $("#reservationTerminalName").keyup(function () {
            $("#reservationTerminalNameId").val(null);
        });

        $("#reservationAerolineName").click(function () {
            $("#reservationAerolineName").autocomplete("search", "");
        });

        $("#reservationAerolineName").keyup(function () {
            $("#reservationAerolineNameId").val(null);
        });
    });

    function getAirport() {
        var airports = [];

        $.getJSON(resolveUrl("~/Airport/GetAirports"), function (data) {
            $.each(data.data, function (key, val) {
                var airportModel = {
                    label: val.name,
                    value: val.code,
                    id: val.id
                };
                airports.push(airportModel);
            });
        });

        $("#toAirportInformation").autocomplete({
            minLength: 0,
            source: airports,
            select: function (event, ui) {
                event.preventDefault();
                CleanInputWhenAirportIsFiredUp();
                $("#toAirportInformation").val(ui.item.label);
                $("#toAirportId").val(ui.item.value);
                CallTerminalByAirport(ui);
                return false;
            }

        });

        $("#fromAirportInformation").autocomplete({
            minLength: 0,
            source: airports,
            select: function (event, ui) {
                event.preventDefault();
                CleanInputWhenAirportIsFiredUp();
                $("#fromAirportInformation").val(ui.item.label);
                $("#fromAirportId").val(ui.item.value);
                CallTerminalByAirport(ui);
                return false;
            }
        });
    }

    function CallTerminalByAirport(airportInformation) {
        var terminalsByAirport = [];

        $.ajax({
            url: resolveUrl("~/Airport/GetTerminalsByAirport"),
            data: {
                airportCode: airportInformation.item.value
            },
            type: "Get",
            success: function (response) {
                $.each(response, function(key, value) {
                    var terminal = {
                        label: value.name,
                        value: value.id
                    };

                    terminalsByAirport.push(terminal);
                });
            }
        });

        $("#reservationTerminalName").autocomplete({
            minLength: 0,
            source: terminalsByAirport,
            select: function (event, ui) {
                event.preventDefault();
                $("#reservationAerolineName").val('');
                $("#reservationAerolineNameId").val(null);
                $("#reservationTerminalName").val(ui.item.label);
                $("#reservationTerminalNameId").val(ui.item.value);
                CallAirlinesByTerminal(ui);
                return false;
            }

        });
    }

    function CallAirlinesByTerminal(terminalInformation) {
        var airlinesByTerminal = [];

        $.ajax({
            url: resolveUrl("~/Airport/GetAirlinesByTerminal"),
            data: {
                terminalId: terminalInformation.item.value
            },
            type: "Get",
            success: function (response) {
                $.each(response, function (key, value) {
                    var airline = {
                        label: value.name,
                        value: value.id
                    };

                    airlinesByTerminal.push(airline);
                });
            }
        });

        $("#reservationAerolineName").autocomplete({
            minLength: 0,
            source: airlinesByTerminal,
            select: function (event, ui) {
                event.preventDefault();
                $("#reservationAerolineName").val(ui.item.label);
                $("#reservationAerolineNameId").val(ui.item.value);
                return false;
            }

        });
    }

    function CleanInputWhenAirportIsFiredUp() {
        $("#reservationAerolineName").val('');
        $("#reservationAerolineNameId").val(null);

        $("#reservationTerminalName").val('');
        $("#reservationTerminalNameId").val(null);
    }

    return {
    };
}());
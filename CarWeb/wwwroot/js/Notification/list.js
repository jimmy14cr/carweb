﻿var notificationList = (function () {

    var rejectNotification = function (id, name) {
        var message = "¿Do you want to reject this reservation?";
        if (name !== undefined) {
            message = "¿Do you want to reject this reservation " + name + "?";
        }


        bootbox.setLocale("es");
        bootbox.confirm({
            title: "",
            message: message,
            buttons: {
                cancel: {
                    label: 'Cancel'
                },
                confirm: {
                    label: ' Confirm'
                }
            },
            callback: function (result) {

                if (result) {
                    var ajaxMethodUrl = resolveUrl("~/Notification/Reject");
                    if (id === undefined || id === null) {
                        id = $("#txt-id-sickness").val();
                    }
                    $.ajax({
                        async: true,
                        cache: false,
                        type: "Post",
                        url: ajaxMethodUrl,
                        data: {
                            id: id,
                            // __RequestVerificationToken: $("form").find("input[name='__RequestVerificationToken']").val()
                        }
                    }).done(function (resultData) {

                        if (resultData.isValid) {
                            bootbox.alert({
                                size: "small",
                                title: "",
                                message: resultData.message,
                                animate: true,
                                callback: function () {
                                    if (id === undefined || id === null) {
                                        window.location = resolveUrl("~/notification");
                                    } else {
                                        var table = $("#reservationTable").DataTable();
                                        table.search();
                                        table.ajax.reload();
                                    }
                                }
                            });

                        } else {
                            bootbox.alert({
                                size: "small",
                                title: "Reservation",
                                message: resultData.errorMessage,
                                animate: true
                            });
                        }

                    }).fail(function (resultData) {

                        bootbox.alert({
                            size: "small",
                            title: "Reservation",
                            message: "Error rejecting the reservation.",
                            animate: true
                        });
                    });
                }
            }
        });
    };

    var acceptNotification = function (id, name) {
        var message = "¿Do you want to accept this reservation?";


        bootbox.setLocale("es");
        bootbox.confirm({
            title: "",
            message: message,
            buttons: {
                cancel: {
                    label: 'Cancel'
                },
                confirm: {
                    label: ' Confirm'
                }
            },
            callback: function (result) {

                if (result) {
                    var ajaxMethodUrl = resolveUrl("~/Notification/Accept");
                    if (id === undefined || id === null) {
                        id = $("#txt-id-sickness").val();
                    }
                    $.ajax({
                        async: true,
                        cache: false,
                        type: "post",
                        url: ajaxMethodUrl,
                        data: {
                            id: id
                        }
                    }).done(function (resultData) {
                        if (resultData.isValid) {
                            bootbox.alert({
                                size: "small",
                                title: "",
                                message: resultData.message,
                                animate: true,
                                callback: function () {
                                    if (id === undefined || id === null) {
                                        window.location = resolveUrl("~/notification");
                                    } else {
                                        var table = $("#reservationTable").DataTable();
                                        table.search();
                                        table.ajax.reload();
                                    }
                                }
                            });
                        } else {
                            bootbox.alert({
                                size: "small",
                                title: "Reservation",
                                message: resultData.errorMessage,
                                animate: true
                            });
                        }
                    }).fail(function (resultData) {

                        bootbox.alert({
                            size: "small",
                            title: "Reservation",
                            message: "Error accepting the reservation.",
                            animate: true
                        });
                    });
                }
            }
        });
    };

    $(document).ready(function () {

        $('#reservationTable').DataTable({
            "processing": true,
            "serverSide": false,
            "paging": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/English.json"
            },
            "ajax": resolveUrl("~/Notification/List"),
            "columns": [
                //{
                //    "orderable": true,
                //    "data": "idUserNavigation.name",
                //    "render": function (data, type, row, meta) {
                //        return data;
                //    }
                //},
                {
                    "orderable": true,
                    "data": "idCarNavigation.name",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "orderable": true,
                    "data": "pickUpLocation",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "orderable": true,
                    "data": "dropOffLocation",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },
                {
                    "orderable": true,
                    "data": "pickUpDate",
                    "render": function (data, type, row, meta) {
                        return moment(data).format("MM-DD-YYYY");
                    }
                },
                {
                    "orderable": true,
                    "data": "pickUpTime",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },

                {
                    "orderable": true,
                    "data": "idStatusNavigation.name",
                    "render": function (data, type, row, meta) {
                        return data;
                    }
                },

                {
                    "orderable": false,
                    "data": "id",
                    "width": "5%",
                    "render": function (data, type, row, meta) {
                        var html = "";
                        var notificationColumnName = $("#notificationColumnName");

                        console.log($("#isUserAdmin").val());
                        html = "<sdf style='display: flex; padding: 0px; margin: 0px'>";
                        if ($("#isUserAdmin").val() === "true") {
                            notificationColumnName.text('Actions');
                            if (row.idStatusNavigation.name == 'Accepted') {
                                html += "<a title='Acept' style='margin-right: 5px; color: #FFF;' class='btn btn-success disabled' onclick='notificationList.acceptNotification(\"" + data + "\")'>Accept</a>";
                                html += "<a title='Reject' class='btn btn-danger' style='margin-right: 5px; color: #FFF;' onclick='notificationList.rejectNotification(\"" + data + "\")'>Reject</a>";
                            } if (row.idStatusNavigation.name == 'Cancelled') {
                                html += "<a title='Acept' style='margin-right: 5px; color: #FFF;' class='btn btn-success' onclick='notificationList.acceptNotification(\"" + data + "\")'>Accept</a>";
                                html += "<a title='Reject' class='btn btn-danger disabled' style='margin-right: 5px; color: #FFF;' onclick='notificationList.rejectNotification(\"" + data + "\")'>Reject</a>";
                            } if (row.idStatusNavigation.name == 'Pending') {
                                html += "<a title='Acept' style='margin-right: 5px; color: #FFF;' class='btn btn-success' onclick='notificationList.acceptNotification(\"" + data + "\")'>Accept</a>";
                                html += "<a title='Reject' class='btn btn-danger' style='margin-right: 5px; color: #FFF;' onclick='notificationList.rejectNotification(\"" + data + "\")'>Reject</a>";
                            }              
                        }


                        html += "<a style='margin-right: 5px; background-color: #007bff; border-color: #007bff;' class='btn btn-info btn-xs' href='" + resolveUrl("~/Notification/Preview/" + data) + "'>Preview</a>";
                        html += "</div>";
                        return html;
                    }
                }
            ],
            "order": [[0, 'asc']],
            dom: 'Bfrtip'
        });
    });

    return {
        rejectNotification: rejectNotification,
        acceptNotification: acceptNotification
    };
}());
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Encryption2;
using CarWeb.Extensions;
using CarWeb.Models.ViewModels.User;
using CarWeb.Security;
using CarWeb.Services.Interfaces;

namespace CarWeb.Controllers
{
    [Authorize]
    public class SecurityController : Controller
    {

        private readonly ILogger<SecurityController> _logger;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public SecurityController(ILogger<SecurityController> logger,
            IUserService userService,
            IMapper mapper,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            var r = Encryption.DecryptTripleDes("B1Ty3HY0vuibmiP/TmJnnUH7BXE2BJJGBAapUoE74tI="); //rjg2019&human_code_2019&
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {

                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: true, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var userLoging = await _userManager.FindByNameAsync(model.Email);
                    HttpContext.SetSession(userLoging.Id);

                    _logger.LogInformation(1, "User logged in.");

                    return RedirectToAction(nameof(NotificationController.Index), "Notification");
                }

                ModelState.AddModelError(string.Empty, "Invalid Credentials");
                return View(model);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            //Limpiar todas las variables de session para que no se cargue ninguna
            HttpContext.Session.Clear();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction(nameof(Login), "Security");
        }

        public IActionResult AccessDenied()
        {
            var model = new UserViewModel();
            return View(model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(NotificationController.Index), "Notification");
            }
        }
    }
}
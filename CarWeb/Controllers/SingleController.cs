﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Models;

namespace CarWeb.Controllers
{
    public class SingleController : Controller
    {
        private readonly ILogger<SingleController> _logger;
        private readonly IMapper _mapper;

        public SingleController(ILogger<SingleController> logger,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}

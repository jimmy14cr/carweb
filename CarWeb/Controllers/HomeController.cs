﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Mapping;
using CarWeb.Models;
using CarWeb.Models.Car;
using CarWeb.Models.Messaging.User;
using CarWeb.Models.User;
using CarWeb.Models.ViewModels.Reservation;
using CarWeb.Models.ViewModels.User;
using CarWeb.Models.ViewModels.Car;
using CarWeb.Services.Interfaces;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using CarWeb.Security;
using CarWeb.Extensions;
using CarWeb.Models.ViewModels.ServiceType;
using CarWeb.Utils;
using CarWeb.Models.DataTable;
using CarWeb.Models.ViewModels.CountryCode;
using CarWeb.Services.Interfaces.EmailSender;

namespace CarWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICarService _carService;
        private readonly IReservationService _reservationService;
        private readonly IServiceTypeService _serviceTypeService;
        private readonly ICountryCodeService _countryCodeService;
        private IEmailSenderService _emailSenderService;
        private readonly IUserService _userService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;



        public HomeController(ILogger<HomeController> logger, ICarService carService,
            IReservationService reservationService, IUserService userService, IMapper mapper,
            SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager,
            IServiceTypeService serviceTypeService, ICountryCodeService countryCodeService, IEmailSenderService emailSenderService)
        {
            _logger = logger;
            _carService = carService;
            _userService = userService;
            _reservationService = reservationService;
            _mapper = mapper;
            _signInManager = signInManager;
            _userManager = userManager;
            _serviceTypeService = serviceTypeService;
            _countryCodeService = countryCodeService;
            _emailSenderService = emailSenderService;
        }

        public IActionResult Covid()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            //var carList = await _carService.GetAll(new ContractRequest<BaseRequest>());

            //var reservation = new ReservationViewModel();
            //await SetupViewModel(reservation);

            //var model = new Tuple<List<CarViewModel>, ReservationViewModel>(carList.Data.Cars.ToList(), reservation);

            return View();
        }

        private async Task SetupViewModel(ReservationViewModel model)
        {
            var serviceTypes = await _serviceTypeService.GetAll(new ContractRequest<BaseRequest>());
            var newServiceTypes = serviceTypes.Data.ServiceTypes.ToList();
            var defaultServiceTypes = newServiceTypes.Any() ? newServiceTypes.FirstOrDefault() : new ServiceTypeViewModel();

            model.ServiceTypes =
                DropDownUtil.GetDropDownPappingGeneric(
                    newServiceTypes,
                    nameof(defaultServiceTypes.Code),
                    nameof(defaultServiceTypes.Name)
                );
        }

        [HttpGet]
        public async Task<IActionResult> CountryCodeList()
        {
            try
            {
                var reservationResponse = await _countryCodeService.GetAll(new ContractRequest<BaseRequest>());

                var response = new DataTableResponseViewModel<CountryCodeViewModel>
                {
                    Data = reservationResponse.Data.CountryCodes.ToList(),
                    RecordsFiltered = reservationResponse.Data.CountryCodes.Count(),
                    RecordsTotal = reservationResponse.Data.CountryCodes.Count()
                };

                return Json(response);

            }
            catch (Exception ex)
            {
                var response = new DataTableResponseViewModel<ReservationViewModel>
                {
                    Data = new List<ReservationViewModel>(),
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Error = $"Error getting Parameters: {ex.Message}"
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCarAvaliable(string departureDate, string arriveDate)
        {
            try
            {
                var cars = await _carService.GetByVerify(new ContractRequest<BaseRequest> { Data = new BaseRequest { ArriveDate = arriveDate, DepartureDate = departureDate } });
                var modelList = cars.Data.Cars;
                if (modelList.ToList().Count == 0)
                {
                    return Json(null);
                }
                else
                {
                    return Json(modelList.ToList());
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting SellerGroups");
                return Json(null);
            }
        }


        [HttpPost]
        public async Task<IActionResult> AddPerson([FromBody] UserViewModel request)
        {
            try
            {
                var requestUser = request.ToContractRequest(_mapper);
                var userAdd = await _userService.Add(requestUser);
                return Json(userAdd.Data.User);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting Reservation");
                return Json(null);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveNewPassword([FromBody] UserViewModel request)
        {
            try
            {
                var requestUser = request.ToContractRequest(_mapper);
                var userAdd = await _userService.UpdataPassword(requestUser);

                var logUser = await Authenticate(userAdd.Data.User.Email, request.Password);

                return Json(userAdd.Data.User);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting Reservation");
                return Json(null);
            }
        }

        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> AddReservation([FromBody] ReservationViewModel request)
        {
            try
            {
                var requestReservation = request.ToContractRequest(_mapper);
                var reservationAdd = await _reservationService.Add(requestReservation);
                return Json(reservationAdd.Data.Reservation);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting Reservation");
                return Json(null);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Preview(Guid id)
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<bool> Authenticate(string email, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, isPersistent: true, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var userLoging = await _userManager.FindByNameAsync(email);
                HttpContext.SetSession(userLoging.Id);

                _logger.LogInformation(1, "User logged in.");

                return true;
            }

            return false;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] UserViewModel request)
        {
            var result = await _signInManager.PasswordSignInAsync(request.Email, request.Password, isPersistent: true, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var userLoging = await _userManager.FindByNameAsync(request.Email);
                HttpContext.SetSession(userLoging.Id);

                _logger.LogInformation(1, "User logged in.");

                return Json(true);
            }

            return Json(false);
        }

        [HttpGet]
        public async Task<IActionResult> IsUserLogged()
        {
            bool isAuthenticated = HttpContext.User.Identity.IsAuthenticated;

            return Json(isAuthenticated);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Extensions;
using CarWeb.Models;
using CarWeb.Models.Messaging.Reservation;
using CarWeb.Models.ViewModels.DataTable;
using CarWeb.Models.ViewModels.Reservation;
using CarWeb.Services.Interfaces;

namespace CarWeb.Controllers
{
    public class NotificationController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IReservationService _reservationService;
        private readonly IMapper _mapper;

        public NotificationController(ILogger<HomeController> logger,
            IReservationService reservationService, IMapper mapper)
        {
            _logger = logger;
            _reservationService = reservationService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            try
            {
                var idUser = HttpContext.GetSession().Id;
                var reservationResponse = await _reservationService.GetAll(new ContractRequest<BaseRequest> { Data = new BaseRequest { IdUser = idUser } });

                var response = new DataTableResponseViewModel<ReservationViewModel>
                {
                    Data = reservationResponse.Data.Reservations.ToList(),
                    RecordsFiltered = reservationResponse.Data.Reservations.Count(),
                    RecordsTotal = reservationResponse.Data.Reservations.Count()
                };

                return Json(response);

            }
            catch (Exception ex)
            {
                var response = new DataTableResponseViewModel<ReservationViewModel>
                {
                    Data = new List<ReservationViewModel>(),
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Error = $"Error getting Parameters: {ex.Message}"
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Preview(Guid id)
        {
            var model = await _reservationService.Preview(new ContractRequest<BaseRequest> { Data = new BaseRequest { Id = id } });

            //var modelMapped = _mapper.Map<ReservationViewModel>(model.Data.ReservationPreview);

            return View(model.Data.ReservationPreview);
        }

        [HttpPost]
        public IActionResult Accept(ReservationGetRequest request)
        {
            var result = new JsonResultViewModel<string>
            {
                IsValid = true,
                Message = $"Accepted."
            };

            try
            {
                var model = _mapper.Map<ReservationViewModel>(new ReservationViewModel
                {
                    Id = request.Id
                });

                var response = _reservationService.Accept(new ContractRequest<ReservationAddRequest> { Data = new ReservationAddRequest { Reservation = model } });

                if (response == null)
                {
                    result.IsValid = false;
                    result.Message = $"The reservation has not been accepted.";

                    return Json(result);
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message;
            }
            result.IsValid = false;
            result.Message = $"The reservation has not been accepted.";

            return Json(result);
        }

        [HttpPost]
        public IActionResult Reject(ReservationGetRequest request)
        {
            var result = new JsonResultViewModel<string>
            {
                IsValid = true,
                Message = $"Rejected."
            };

            try
            {
                var model = _mapper.Map<ReservationViewModel>(new ReservationViewModel
                {
                    Id = request.Id
                });

                var response = _reservationService.Reject(new ContractRequest<ReservationAddRequest> { Data = new ReservationAddRequest { Reservation = model } });

                if (response == null)
                {
                    result.IsValid = false;
                    result.Message = $"The reservation has not been rejected.";

                    return Json(result);
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message;
            }
            result.IsValid = false;
            result.Message = $"The reservation has not been rejected.";

            return Json(result);
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
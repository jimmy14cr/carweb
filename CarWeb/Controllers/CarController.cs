﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Models;
using CarWeb.Models.Car;
using CarWeb.Services.Interfaces;

namespace CarWeb.Controllers
{
    public class CarController : Controller
    {
        private readonly ILogger<CarController> _logger;
        private readonly IMapper _mapper;
        private readonly ICarService _carService;

        public CarController(ILogger<CarController> logger, ICarService carService, IMapper mapper)
        {
            _carService = carService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var carList = await _carService.GetAll(new ContractRequest<BaseRequest>());
            return View(carList.Data.Cars);
        }
    }
}

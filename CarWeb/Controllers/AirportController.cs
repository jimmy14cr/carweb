﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Models;
using CarWeb.Models.DataTable;
using CarWeb.Models.Messaging.Airline;
using CarWeb.Models.Messaging.Terminal;
using CarWeb.Models.ViewModels.Airport;
using CarWeb.Models.ViewModels.Terminal;
using CarWeb.Services.Interfaces;

namespace CarWeb.Controllers
{
    public class AirportController : Controller
    {
        private readonly ILogger<AirportController> _logger;
        private readonly IMapper _mapper;
        private readonly IAirportService _airportService;

        public AirportController(ILogger<AirportController> logger, IAirportService airportService, IMapper mapper)
        {
            _airportService = airportService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAirports()
        {
            try
            {
                var reservationResponse = await _airportService.GetAirports(new ContractRequest<BaseRequest>());

                var response = new DataTableResponseViewModel<AirportViewModel>
                {
                    Data = reservationResponse.Data.Airports.ToList(),
                    RecordsFiltered = reservationResponse.Data.Airports.Count(),
                    RecordsTotal = reservationResponse.Data.Airports.Count()
                };

                return Json(response);

            }
            catch (Exception ex)
            {
                var response = new DataTableResponseViewModel<AirportViewModel>
                {
                    Data = new List<AirportViewModel>(),
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Error = $"Error getting Parameters: {ex.Message}"
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTerminalsByAirport(string airportCode)
        {
            try
            {
                var reservationResponse = await _airportService
                    .GetTerminalsByAirport(new ContractRequest<TerminalGetRequest> { Data = new TerminalGetRequest { AirportCode = airportCode } });

                return Json(reservationResponse.Data.Terminals.ToList());
            }
            catch (Exception ex)
            {
                var response = new DataTableResponseViewModel<AirportViewModel>
                {
                    Data = new List<AirportViewModel>(),
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Error = $"Error getting Parameters: {ex.Message}"
                };
                return Json(response);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAirlinesByTerminal(Guid terminalId)
        {
            try
            {
                var reservationResponse = await _airportService
                    .GetAirlinesByTerminal(new ContractRequest<AirlineGetRequest> { Data = new AirlineGetRequest { TerminalId = terminalId } });

                return Json(reservationResponse.Data.Airlines.ToList());
            }
            catch (Exception ex)
            {
                var response = new DataTableResponseViewModel<AirportViewModel>
                {
                    Data = new List<AirportViewModel>(),
                    RecordsFiltered = 0,
                    RecordsTotal = 0,
                    Error = $"Error getting Parameters: {ex.Message}"
                };
                return Json(response);
            }
        }
    }
}

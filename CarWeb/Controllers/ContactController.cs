﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CarWeb.Mapping;
using CarWeb.Models;
using CarWeb.Models.ViewModels.Contact;
using CarWeb.Services.Interfaces.Contact;

namespace CarWeb.Controllers
{
    public class ContactController : Controller
    {
        private readonly ILogger<ContactController> _logger;
        private readonly IMapper _mapper;
        private readonly IContactService _contactService;

        public ContactController(ILogger<ContactController> logger, IContactService contactService,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _contactService = contactService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> AddContact([FromBody] ContactViewModel request)
        {
            try
            {
                var requestContact = request.ToContractRequest(_mapper);
                var contactAdd = await _contactService.Add(requestContact);
                return Json(contactAdd.Data.Contact);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Getting Contact");
                return Json(null);
            }
        }
    }
}

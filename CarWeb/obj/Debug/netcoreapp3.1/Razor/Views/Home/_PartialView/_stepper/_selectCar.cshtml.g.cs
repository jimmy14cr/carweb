#pragma checksum "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6ea5b9c0ea00f9d711b8562f4fc600f97a213283"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home__PartialView__stepper__selectCar), @"mvc.1.0.view", @"/Views/Home/_PartialView/_stepper/_selectCar.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models.Car;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6ea5b9c0ea00f9d711b8562f4fc600f97a213283", @"/Views/Home/_PartialView/_stepper/_selectCar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"283d6fc0ea6fcd803230c41dc6add4e352f5b02b", @"/Views/_ViewImports.cshtml")]
    public class Views_Home__PartialView__stepper__selectCar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Tuple<List<CarWeb.Models.ViewModels.Car.CarViewModel>, CarWeb.Models.ViewModels.Reservation.ReservationViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div id=""selectCar"" class=""content"" role=""tabpanel"" aria-labelledby=""selectCar-trigger"">
    <div class=""col-lg-12"">
        <div class=""row align-items-center mb-4"">
            <div class=""col-md-6"">
                <h3 class=""m-0"">Select a car</h3>
            </div>
        </div>
        <div class=""row"">
            <div class=""col-lg-1 align-self-center"">
                <a href=""#"" class=""btn btn-primary custom-prev""><</a>
            </div>
            <div class=""col-lg-10"">
                <div class=""nonloop-block-13 owl-carousel"" id=""carouselCar"">
");
#nullable restore
#line 15 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
                     foreach (var item in Model.Item1)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div class=\"item-1 item-2\" data-target-id=\"");
#nullable restore
#line 17 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
                                                              Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"");
            BeginWriteAttribute("id", " id=\"", 857, "\"", 870, 1);
#nullable restore
#line 17 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
WriteAttributeValue("", 862, item.Id, 862, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                            <div data-toggle=\"modal\" data-target=\"#modalCar\">\r\n                                <img");
            BeginWriteAttribute("src", " src=", 989, "", 1031, 1);
#nullable restore
#line 19 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
WriteAttributeValue("", 994, item.ImageCars.FirstOrDefault().Path, 994, 37, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"Image\" class=\"img-fluid\">\r\n                                <div class=\"item-1-contents\">\r\n                                    <div class=\"text-center\">\r\n                                        <h3><a style=\"white-space: nowrap\">");
#nullable restore
#line 22 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
                                                                      Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a></h3>
                                    </div>
                                </div>
                            </div>
                            <div class=""d-flex action justify-content-center align-content-center"" >
                                <a class=""btn btn-primary rentCar text-white"" data-target-id=""");
#nullable restore
#line 27 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
                                                                                         Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\"");
            BeginWriteAttribute("id", " id=\"", 1611, "\"", 1624, 1);
#nullable restore
#line 27 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
WriteAttributeValue("", 1616, item.Id, 1616, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Rent</a>\r\n                            </div>\r\n                        </div>\r\n");
#nullable restore
#line 30 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"

                    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                </div>
            </div>
            <div class=""col-lg-1 align-self-center"">
                <a href=""#"" class=""btn btn-primary custom-next"">></a>
            </div>
        </div>
    </div>
</div>

<script type=""text/javascript"">
    var carList = ");
#nullable restore
#line 42 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\_PartialView\_stepper\_selectCar.cshtml"
              Write(Json.Serialize(Model.Item1));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Tuple<List<CarWeb.Models.ViewModels.Car.CarViewModel>, CarWeb.Models.ViewModels.Reservation.ReservationViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591

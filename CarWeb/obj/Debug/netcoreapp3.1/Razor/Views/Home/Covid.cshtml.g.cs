#pragma checksum "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Home\Covid.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cdc7261a074216bf341e90463089cf4a969de98c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Covid), @"mvc.1.0.view", @"/Views/Home/Covid.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models.Car;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cdc7261a074216bf341e90463089cf4a969de98c", @"/Views/Home/Covid.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"283d6fc0ea6fcd803230c41dc6add4e352f5b02b", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Covid : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<div id=""content"" class=""main-content"">
    <div class=""sqs-layout sqs-grid-12 columns-12"">
<div  style=""padding: 15% 10% 10% 5%;"">
<div class=""col sqs-col-12 span-12"">
<div class=""sqs-block html-block sqs-block-html"" data-block-type=""2"" id=""block-5f48ba50436c6d5fb06256de"">
<div class=""sqs-block-content"">
<h3 style=""text-align:center;white-space:pre-wrap;"">#STOPTHESPREAD</h3>
<h1 style=""text-align:center;white-space:pre-wrap;"">
<strong>COVID-19 Update</strong></h1>
<p style=""text-align:center;white-space:pre-wrap;""");
            BeginWriteAttribute("class", " class=\"", 531, "\"", 539, 0);
            EndWriteAttribute();
            WriteLiteral(">As we continue through this life-altering pandemic together, Super City Limousine is committed to providing our industry leading experience while keeping you as safe as possible.</p>\r\n<p style=\"text-align:center;white-space:pre-wrap;\"");
            BeginWriteAttribute("class", " class=\"", 775, "\"", 783, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n<strong>Super City Limousine has always been a “consumer first” company, and through this pandemic, we continue to be so more than ever.</strong></p>\r\n<p style=\"text-align:center;white-space:pre-wrap;\"");
            BeginWriteAttribute("class", " class=\"", 988, "\"", 996, 0);
            EndWriteAttribute();
            WriteLiteral(@">We are closely following and adhering to the Centers for Disease Control’s (CDC) guidelines and recommendations on the steps we can take to help prevent the spread of the virus. We have developed guidelines and instilled specific instructions with our drivers to emphasize the importance of following safety procedures:</p></div></div>
<div class=""sqs-block horizontalrule-block sqs-block-horizontalrule"" data-block-type=""47"" id=""block-yui_3_17_2_1_1598601821812_4153"">
<div class=""sqs-block-content"">
<hr></div></div>
<div class=""sqs-block html-block sqs-block-html"" data-block-type=""2"" id=""block-yui_3_17_2_1_1598601821812_4214"">
<div class=""sqs-block-content"">
<ol data-rte-list=""default"">
<li>
<p");
            BeginWriteAttribute("class", " class=\"", 1706, "\"", 1714, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"white-space:pre-wrap;\">Thoroughly disinfecting the vehicle after every trip, including in-between stops when applicable</p></li>\r\n<li>\r\n<p");
            BeginWriteAttribute("class", " class=\"", 1861, "\"", 1869, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"white-space:pre-wrap;\">Wiping down the seats, door handles (in/out), armrest, head rest, vents, windows and seat-belts, etc by using approved disinfecting wipes </p></li>\r\n<li>\r\n<p");
            BeginWriteAttribute("class", " class=\"", 2058, "\"", 2066, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"white-space:pre-wrap;\">Offering assistance in disinfecting personal luggage/items</p></li>\r\n<li>\r\n<p");
            BeginWriteAttribute("class", " class=\"", 2175, "\"", 2183, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"white-space:pre-wrap;\">Wearing a mask and avoiding physical contact at all times, including at times of pick-up and drop-off</p></li>\r\n<li>\r\n<p");
            BeginWriteAttribute("class", " class=\"", 2335, "\"", 2343, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"white-space:pre-wrap;\">Providing hand sanitizer to all of our riders</p></li>\r\n<li>\r\n<p");
            BeginWriteAttribute("class", " class=\"", 2439, "\"", 2447, 0);
            EndWriteAttribute();
            WriteLiteral(@" style=""white-space:pre-wrap;"">Any other action that relates to the enhanced protection to both our riders and drivers.</p></li></ol></div></div>
<div class=""sqs-block horizontalrule-block sqs-block-horizontalrule"" data-block-type=""47"" id=""block-yui_3_17_2_1_1598601821812_4978"">
<div class=""sqs-block-content"">
<hr></div></div>
<div class=""sqs-block html-block sqs-block-html"" data-block-type=""2"" id=""block-yui_3_17_2_1_1598601821812_5038"">
<div class=""sqs-block-content"">
<p style=""text-align:center;white-space:pre-wrap;""");
            BeginWriteAttribute("class", " class=\"", 2978, "\"", 2986, 0);
            EndWriteAttribute();
            WriteLiteral(@">Additionally, any driver reporting any symptoms of illness are automatically removed from duties and instructed to self-quarantine and contact their health care provider. Drivers are also subject to temperature checks upon the start and end of each work day.</p></div></div>
<div class=""sqs-block horizontalrule-block sqs-block-horizontalrule"" data-block-type=""47"" id=""block-yui_3_17_2_1_1598601821812_5777"">
<div class=""sqs-block-content"">
<hr></div></div>
<div class=""sqs-block html-block sqs-block-html"" data-block-type=""2"" id=""block-yui_3_17_2_1_1598601821812_5837"">
<div class=""sqs-block-content"">
<p style=""text-align:center;white-space:pre-wrap;""");
            BeginWriteAttribute("class", " class=\"", 3647, "\"", 3655, 0);
            EndWriteAttribute();
            WriteLiteral(">Please stay safe. We hope to provide you with an unforgettable experience very soon.</p>\r\n<p style=\"text-align:center;white-space:pre-wrap;\"");
            BeginWriteAttribute("class", " class=\"", 3797, "\"", 3805, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n<strong>Super City Limousine</strong></p></div></div></div></div></div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591

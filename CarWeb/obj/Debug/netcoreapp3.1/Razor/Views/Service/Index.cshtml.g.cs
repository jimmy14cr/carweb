#pragma checksum "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\Service\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "afdb1366a9de049791c629e05e9d52606228b0ba"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Service_Index), @"mvc.1.0.view", @"/Views/Service/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\jimmy\source\repos\CarWeb\CarWeb\Views\_ViewImports.cshtml"
using CarWeb.Models.Car;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"afdb1366a9de049791c629e05e9d52606228b0ba", @"/Views/Service/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"283d6fc0ea6fcd803230c41dc6add4e352f5b02b", @"/Views/_ViewImports.cshtml")]
    public class Views_Service_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<div class=""ftco-blocks-cover-1"">
    <div class=""ftco-cover-1 overlay innerpage"" style=""background-image: url('/images/hero_2.webp')"">
        <div class=""container"">
            <div class=""row align-items-center justify-content-center"">
                <div class=""col-lg-6 text-center"">
                    <h1>Our Services</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=""site-section"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-car""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Car Key</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, labor");
            WriteLiteral(@"iosam.</p>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-valet-1""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Car Key</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, laboriosam.</p>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-key""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Car Key</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obca");
            WriteLiteral(@"ecati, laboriosam.</p>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-car-1""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Repair</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, laboriosam.</p>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-traffic""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Car Accessories</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur a");
            WriteLiteral(@"dipisicing elit. Obcaecati, laboriosam.</p>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 mb-4 mb-lg-5"">
                <div class=""service-1 dark"">
                    <span class=""service-1-icon"">
                        <span class=""flaticon-valet""></span>
                    </span>
                    <div class=""service-1-contents"">
                        <h3>Own a Car</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, laboriosam.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591

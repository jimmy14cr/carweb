﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using CarWeb.Models;
using CarWeb.Models.Messaging.User;
using CarWeb.Models.ViewModels.User;
using CarWeb.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Extensions
{
    public static class SessionExtension
    {

        private static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        private static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) :
                JsonConvert.DeserializeObject<T>(value);
        }

        public static void SetSessionSellerId(this HttpContext context, Guid sellerId)
        {
            context.Session.Set("SellerId", sellerId);

        }

        public static Guid GetSessionSellerId(this HttpContext context)
        {

            var id = context.Session.Get<Guid>("SellerId");
            //if (id == Guid.Empty)
            //{
            //    var user = GetSession(context);
            //    if (user != null && user.UserTaxable != null && user.UserTaxable.Any())
            //    {
            //        id = user.UserTaxable.FirstOrDefault().TaxableId;
            //        Set<Guid>(context.Session, "TaxableId", id);
            //    }
            //    else return Guid.Empty;
            //}

            return id;
        }

        public static void SetSession(this HttpContext context, Guid id)
        {
            context.Session.SetString("Id", id.ToString());

        }

        public static UserViewModel GetSession(this HttpContext context)
        {
            var session = context.Session;
            var result = session.GetString("Id");
            //if (!string.IsNullOrEmpty(result))
            //{
            //    throw new NotImplementedException();
            //}


            var identity = context.User.Identity;
            if (!identity.IsAuthenticated)
            {
                throw new NotImplementedException();
            }

            if (!(context.RequestServices.GetService(typeof(IUserService)) is IUserService _userService))
            {
                throw new NotImplementedException();
            }

            var userResponse = _userService.GetById(new ContractRequest<BaseRequest> { Data = new BaseRequest { Id = Guid.Parse(result) } }).GetAwaiter().GetResult();


            if (userResponse == null)
            {
                throw new NotImplementedException();
            }

            var response = new UserViewModel
            {
                Id = userResponse.Data.User.Id,
                Password = userResponse.Data.User.Password,
                UpdateDate = userResponse.Data.User.UpdateDate,
                Email = userResponse.Data.User.Email,
                CreateDate = userResponse.Data.User.CreateDate
            };

            session.SetString("Id", userResponse.Data.User.Id.ToString());
            return response;

        }
    }
}

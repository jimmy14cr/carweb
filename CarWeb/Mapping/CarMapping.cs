﻿

using AutoMapper;
using CarWeb.Models;
using CarWeb.Models.Messaging.Car;
using CarWeb.Models.Car;
using CarWeb.Models.ViewModels.Car;

namespace CarWeb.Mapping
{
    public static class CarMapping
    {
        public static CarViewModel ToCarViewModel(this CarView view, IMapper mapper)
        {
            return mapper.Map<CarViewModel>(view);
        }

        public static CarView ToCarView(this CarViewModel viewModel, IMapper mapper)
        {
            return mapper.Map<CarView>(viewModel);
        }

        public static ContractRequest<CarAddRequest> ToContractRequest(this CarViewModel viewModel, IMapper mapper)
        {
            return new ContractRequest<CarAddRequest>
            {
                Data = new CarAddRequest()
                {
                    Car = mapper.Map<CarViewModel>(viewModel)
                }
            };
        }
    }
}

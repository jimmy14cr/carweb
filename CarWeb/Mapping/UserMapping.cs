﻿

using AutoMapper;
using CarWeb.Models;
using CarWeb.Models.Messaging.User;
using CarWeb.Models.User;
using CarWeb.Models.ViewModels.User;

namespace CarWeb.Mapping
{
    public static class UserMapping
    {
        public static UserViewModel ToUserViewModel(this UserView view, IMapper mapper)
        {
            return mapper.Map<UserViewModel>(view);
        }

        public static UserView ToUserView(this UserViewModel viewModel, IMapper mapper)
        {
            return mapper.Map<UserView>(viewModel);
        }

        public static ContractRequest<UserAddRequest> ToContractRequest(this UserViewModel viewModel, IMapper mapper)
        {
            return new ContractRequest<UserAddRequest>
            {
                Data = new UserAddRequest()
                {
                    User = mapper.Map<UserViewModel>(viewModel)
                }
            };
        }
    }
}

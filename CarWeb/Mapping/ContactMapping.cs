﻿using AutoMapper;
using CarWeb.Models;
using CarWeb.Models.Messaging.Contact;
using CarWeb.Models.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarWeb.Mapping
{
    public static class ContactMapping
    {
        public static ContactViewModel ToContactViewModel(this ContactView view, IMapper mapper)
        {
            return mapper.Map<ContactViewModel>(view);
        }

        public static ContactView ToContactView(this ContactViewModel viewModel, IMapper mapper)
        {
            return mapper.Map<ContactView>(viewModel);
        }

        public static ContractRequest<ContactAddRequest> ToContractRequest(this ContactViewModel viewModel, IMapper mapper)
        {
            return new ContractRequest<ContactAddRequest>
            {
                Data = new ContactAddRequest()
                {
                    Contact = mapper.Map<ContactViewModel>(viewModel)
                }
            };
        }
    }
}
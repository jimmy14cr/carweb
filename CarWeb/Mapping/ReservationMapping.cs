﻿

using AutoMapper;
using CarWeb.Models;
using CarWeb.Models.Messaging.Reservation;
using CarWeb.Models.Reservation;
using CarWeb.Models.ViewModels.Reservation;

namespace CarWeb.Mapping
{
    public static class ReservationMapping
    {
        public static ReservationViewModel ToReservationViewModel(this ReservationView view, IMapper mapper)
        {
            return mapper.Map<ReservationViewModel>(view);
        }

        public static ReservationView ToReservationView(this ReservationViewModel viewModel, IMapper mapper)
        {
            return mapper.Map<ReservationView>(viewModel);
        }

        public static ContractRequest<ReservationAddRequest> ToContractRequest(this ReservationViewModel viewModel, IMapper mapper)
        {
            return new ContractRequest<ReservationAddRequest>
            {
                Data = new ReservationAddRequest()
                {
                    Reservation = mapper.Map<ReservationViewModel>(viewModel)
                }
            };
        }
    }
}
